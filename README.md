# lsdl/protogen

Prototype implementation of LSDL research project using Blobfish PHP framework

## Usage

Require:

`composer require lsdl/protogen`

Execute:

`vendor/bin/generate.php`


