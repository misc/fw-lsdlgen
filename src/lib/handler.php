<?php
  namespace LSDL\protogen\lib;

  /**
   * Trait Handler
   * @author  Martin Springwald <martin.springwald@informatik.tu-chemnitz.de>
   * @license   Greenscale Open Source License
   */
  trait Handler {
    function handle_prefix ($value) {
      $this->prefix = $value;
      $this->ensure_database_dir($this->prefix);
    }

    function handle_vendor ($value) {
      $this->vendor = $value;
    }

    function handle_serial ($value) {
      if ($value != null) {
        $this->serial = $value;
      }
      else {
        $this->serial = uniqid();
      }
    }

    function handle_datalists ($value) {
      foreach($value as $domain=>$desc) {
        $domain_desc = (object) array(
          "text_fields" => ["value"],
          "preview_fields" => ["id", "value"]
        );
        $this->populate_domain($domain, $domain_desc);
        $this->make_dao($domain, $this->get_ucdomstr($domain), $this->prefix."_".$domain, $domain_desc);
        $this->datalists->{$domain} = $desc;
      }
    }

    function handle_domains ($value) {
      foreach($value as $domain=>$desc) {
        $this->populate_domain($domain, $desc);
        if (!(isset($desc->virtual)&&$desc->virtual)) $this->make_dao($domain, ucfirst($domain), $this->prefix."_".$domain, $desc);
        if (isset($desc->aliases)) $this->aliases->{$domain} = $desc->aliases;
      }
    }

    function handle_relations ($value) {
      foreach($value as $domain=>$desc) {
        $table_name = $this->prefix."_".$domain;
        $left_table = explode("_", $domain)[0];
        $right_table = explode("_", $domain)[1];
        $relation = $this->get_relation($domain);
        $this->populate_domain($domain, $desc);
        if (!(isset($desc->virtual)&&$desc->virtual)) $this->make_dao($domain, ucfirst($left_table).ucfirst($right_table).ucfirst($relation),
        $table_name, $desc, "", $left_table, $right_table);
      }
    }

    function handle_views ($value) {
      foreach($value as $domain=>$desc) {
        $table_name = $this->prefix."_".$domain;
        $master = $this->get_master($desc);
        $tables = $this->get_joins($desc);
        $this->populate_domain($domain, $desc);
        $this->make_dao($domain, $this->get_ucdomstr($domain), $table_name, $desc, $master."_", $domain, $tables);
      }
    }

    function handle_forms ($value) {
      foreach($value as $form=>$desc) {
        $this->make_form($form, $desc);
      }
    }

    function handle_lists ($value) {
      foreach($value as $list=>$desc) {
        $this->make_list($list, $desc);
      }
    }

    function handle_pages ($value) {
      foreach($value as $page=>$desc) {
        $this->populate_page($page);
        $this->make_page($page, $desc);
      }
    }

    function handle_processes ($value) {
      $this->processes = $value;
    }

    function handle_services ($value) {
      $this->make_controller($value);
    }
  }
?>
