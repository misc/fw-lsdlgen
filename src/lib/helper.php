<?php
	namespace LSDL\protogen\lib;
	
	/**
	 * Trait Helper
	 * @author	Martin Springwald <martin.springwald@informatik.tu-chemnitz.de>
	 * @license		Greenscale Open Source License
	 */
	trait Helper {
		function remove_dir ($dir) {
			$cmd = "rm -rf";
			if (DIRECTORY_SEPARATOR!=='/') {
				$cmd = "rd /S /Q";
			}
			if (is_dir($dir)) {
				@exec($cmd." ".$dir);
			}
		}
	
		function ensure_queries_dir () {
			$this->remove_dir("dist".DIRECTORY_SEPARATOR."queries");
			@mkdir("dist".DIRECTORY_SEPARATOR."queries");
		}

		function ensure_database_dir ($prefix) {
			$this->remove_dir("dist".DIRECTORY_SEPARATOR.$this->prefix);
			@mkdir("dist".DIRECTORY_SEPARATOR.$this->prefix);
			@mkdir("dist".DIRECTORY_SEPARATOR.$this->prefix.DIRECTORY_SEPARATOR."database");
		}

		function populate_domain ($domain, $desc) {
			if (isset($desc->virtual)&&$desc->virtual) {
				$this->models->$domain = $desc;
			}
			else {
				array_push($this->databases, $domain);
				$this->models->$domain = $desc;
				mkdir("dist".DIRECTORY_SEPARATOR."queries".DIRECTORY_SEPARATOR."$domain");
			}
		}
		
		function populate_page ($page) {
			array_push($this->pages, $page);
		}
		
		function get_relation ($domain) {
			if (count(explode("_", $domain))>2) {
				return explode("_", $domain)[2];
			}
			return "";
		}
	
		function get_ucdomstr ($domain) {
			$ucdomstr = "";
			foreach(explode("_", $domain) as $domstr) {
				$ucdomstr .= ucfirst($domstr);
			}
			return $ucdomstr;
		}
		
		function get_master ($desc) {
			return $desc->domains[0];
		}
	}
?>
