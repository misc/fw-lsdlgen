<?php
    namespace LSDL\protogen\lib;

    /**
     * Class SQLTerms
     * @author  Martin Springwald <martin.springwald@informatik.tu-chemnitz.de>
     * @license     Greenscale Open Source License
     */
    class SQLTerms {
        // SQL terms
        const SQL_CREATE_INTRO = "CREATE TABLE %table_name% (id %IPA%";
        const SQL_CREATE_OUTRO = ") %ENC%";
        const SQL_DELETE = "DELETE FROM %table_name%";
        const SQL_DROP_TABLE = "DROP TABLE IF EXISTS %table_name%";
        const SQL_DROP_VIEW = "DROP VIEW IF EXISTS %table_name%";
        const SQL_SELECT = "SELECT %preview_fields% FROM %table_name%";
        const SQL_DISTINCT_SELECT = "SELECT DISTINCT %preview_fields% FROM %table_name%";
        const SQL_LIST = "SELECT %preview_fields% FROM %table_name% %order_by%";
        const SQL_DISTINCT_LIST = "SELECT DISTINCT %preview_fields% FROM %table_name% %order_by%";
        const SQL_CREATE_VIEW = "CREATE VIEW %view_name% AS"." ".self::SQL_LIST;
        const SQL_CREATE_DISTINCT_VIEW = "CREATE VIEW %view_name% AS"." ".self::SQL_DISTINCT_LIST;
        const SQL_CREATE_VIEW_GROUP = "CREATE VIEW %view_name% AS"." ".self::SQL_LIST." GROUP BY %group_by%";
        const SQL_CREATE_DISTINCT_VIEW_GROUP = "CREATE VIEW %view_name% AS"." ".self::SQL_DISTINCT_LIST." GROUP BY %group_by%";
        const SQL_FILTER = self::SQL_DISTINCT_SELECT." WHERE %and %order_by%";
        const SQL_FILTER_GROUP = self::SQL_DISTINCT_SELECT." WHERE %and GROUP BY %n HAVING %and %order_by%";
        const SQL_DATE_TYPE = "DATE";
        const SQL_INTEGER_TYPE = "INTEGER";
        const SQL_KEY_TYPE = "INTEGER %KEY%";
        const SQL_REAL_TYPE = "REAL";
        const SQL_TEXT_TYPE = "TEXT";
        const SQL_TIMESTAMP_TYPE = "TIMESTAMP";
        const SQL_AUTO_TIMESTAMP = self::SQL_TIMESTAMP_TYPE." "."%DTS%";
        const SQL_FOREIGN_KEY = "FOREIGN KEY(%key%)";
        const SQL_REFERENCES = "REFERENCES %fq_table%(id) ON DELETE CASCADE ON UPDATE CASCADE";
        const SQL_TWO_KEYS_UNIQUE = "UNIQUE (%left_table%_id, %right_table%_id)";
        const SQL_SINGLE_KEY_UNIQUE = "UNIQUE (%table%_id)";
    }
?>
