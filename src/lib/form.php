<?php
  namespace LSDL\protogen\lib;

  /**
   * Trait FormGenerator
   * @author  Martin Springwald <martin.springwald@informatik.tu-chemnitz.de>
   * @license     Greenscale Open Source License
   */
  trait FormGenerator {
      private function form_generator_helper_get_autobind_ar ($desc_autobind) {
        $autobind_ar = $desc_autobind;
        if (!is_array($autobind_ar)) {
            $autobind_ar = array($autobind_ar);
        }
        return $autobind_ar;
      }

      private function form_generator_helper_process_feedback ($desc) {
        $feedback = "";
        if (isset($desc->feedback)) {
          if ($desc->feedback == "sticker") {
            $feedback = " data-feedback='sticker'";
          }
        }
        return $feedback;
      }

      private function form_generator_helper_process_await ($desc) {
        $await = "";
        if (isset($desc->await)) {
          $await = " data-await='".$desc->await."'";
        }
        return $await;
      }

      private function form_generator_helper_make_prefix ($field_name, $field_desc) {
        $prefix = "";
        if (@$field_desc->appearance=="block" || @$field_desc->appearance=="blockreverse") {
          $prefix = "<div data-name='".$field_name."' class='inputwrapper'>";
        }
        else if (@$field_desc->appearance=="startblock") {
          $prefix = "<div class='inputblock'><div data-name='".$field_name."' class='inputwrapper'>";
        }
        else if (@$field_desc->appearance=="endblock") {
          $prefix = "<div data-name='".$field_name."' class='inputwrapper'>";
        }
        return $prefix;
      }

      private function form_generator_helper_make_suffix ($field_name, $field_desc) {
        $suffix = "";
        if (@$field_desc->appearance=="block" || @$field_desc->appearance=="blockreverse") {
          $suffix = "</div>";
        }
        else if (@$field_desc->appearance=="startblock") {
          $suffix = "</div>";
        }
        else if (@$field_desc->appearance=="endblock") {
          $suffix = "</div></div>";
        }
        return $suffix;
      }

      private function form_generator_helper_make_extra ($field_desc) {
        $required = "";
        if (@$field_desc->required=="always") {
          $required = " "."required";
        }
        if (isset($field_desc->check)) {
          $required .= " "."data-check='".$field_desc->check."'";
        }
        if (isset($field_desc->mode)) {
          $required .= " "."data-mode='".$field_desc->mode."'";
        }
        if (isset($field_desc->placeholder)) {
          if ($field_desc->placeholder === true) {
            $required .= " "."placeholder='".$field_desc->label."'";
          } else {
            if ($field_desc->placeholder !== false) {
                $required .= " "."placeholder='".$field_desc->placeholder."'";
            }            
          }
        }
        if (isset($field_desc->pattern)) {
           $required .= " "."pattern='".$field_desc->pattern."'";
        }
        return $required;
      }

      private function form_generator_helper_make_select ($id, $field_name, $field_desc, $prefix, $suffix, $required, $bind, $condition, $default, $value, $f_proto) {
        $labelprefix = "";
        $labelsuffix = "";
        $fieldprefix = "";
        $fieldsuffix = "";
        if (@$field_desc->appearance=="box" || @$field_desc->appearance=="boxreverse") {
          $labelprefix = "<div class=\"label_box text\">";
          $labelsuffix = "</div>";
          $fieldprefix = "<div class=\"select_box\">";
          $fieldsuffix = "</div>";
        }
        $f_proto.=$prefix;
        if (@$field_desc->appearance=="boxreverse") {
          $f_proto.=$fieldprefix."<select id='$id-$field_name' name='$field_name'$required$bind$condition$default></select>".$fieldsuffix;
          $f_proto.=$labelprefix."<label for='$id-$field_name'$condition>".$field_desc->label."</label>".$labelsuffix;
        }
        else {
          $f_proto.=$labelprefix."<label for='$id-$field_name'$condition>".$field_desc->label."</label>".$labelsuffix;
          $f_proto.=$fieldprefix."<select id='$id-$field_name' name='$field_name'$required$bind$condition$default></select>".$fieldsuffix;
        }
        if (isset($field_desc->hint)) $f_proto.="<div class=\"hint\" data-name=\"$field_name\">".$field_desc->hint."</div>";
        $f_proto.=$suffix.PHP_EOL;
        return $f_proto;
      }

      private function form_generator_helper_make_text ($id, $field_name, $field_desc, $prefix, $suffix, $required, $bind, $condition, $default, $value, $f_proto) {
        $labelprefix = "";
        $labelsuffix = "";
        $fieldprefix = "";
        $fieldsuffix = "";
        $type = "text";
        if (in_array($field_name, ["verifier", "password"])) {
          $type = "password";
        }
        if (@$field_desc->appearance=="box") {
          $labelprefix = "<div class=\"label_box text\">";
          $labelsuffix = "</div>";
          $fieldprefix = "<div class=\"input_box\">";
          $fieldsuffix = "</div>";
        }
        $f_proto.=$prefix;
        $f_proto.=$labelprefix."<label for='$id-$field_name'$condition>".$field_desc->label."</label>".$labelsuffix;
        $f_proto.=$fieldprefix."<input id='$id-$field_name' type='$type' name='$field_name'$required$value$condition/>".$fieldsuffix;
        if (isset($field_desc->hint)) $f_proto.="<div class=\"hint\" data-name=\"$field_name\">".$field_desc->hint."</div>";
        $f_proto.=$suffix.PHP_EOL;
        return $f_proto;
      }

      private function form_generator_helper_make_geo ($id, $field_name, $field_desc, $prefix, $suffix, $required, $bind, $condition, $default, $value, $f_proto) {
        $labelprefix = "";
        $labelsuffix = "";
        $fieldprefix = "";
        $fieldsuffix = "";
        if (@$field_desc->appearance=="box") {
          $labelprefix = "<div class=\"label_box text\">";
          $labelsuffix = "</div>";
          $fieldprefix = "<div class=\"input_box\">";
          $fieldsuffix = "</div>";
        }
        $f_proto.=$prefix;
        $f_proto.=$labelprefix."<label for='$id-$field_name'$condition>".$field_desc->label.
        "</label>".$labelsuffix.$fieldprefix."<input id='$id-$field_name' type='text' name='$field_name'$required$value$condition/>".$fieldsuffix;
        if (isset($field_desc->hint)) $f_proto.="<div class=\"hint\" data-name=\"$field_name\">".$field_desc->hint."</div>";
        $f_proto.=$suffix.PHP_EOL;
        return $f_proto;
      }

      private function form_generator_helper_make_int ($id, $field_name, $field_desc, $prefix, $suffix, $required, $bind, $condition, $default, $value, $f_proto) {
        $labelprefix = "";
        $labelsuffix = "";
        $fieldprefix = "";
        $fieldsuffix = "";
        if (@$field_desc->appearance=="box") {
          $labelprefix = "<div class=\"label_box\">";
          $labelsuffix = "</div>";
          $fieldprefix = "<div class=\"input_box\">";
          $fieldsuffix = "</div>";
        }
        $f_proto.=$prefix;
        $f_proto.=$labelprefix."<label for='$id-$field_name'$condition>".$field_desc->label.
        "</label>".$labelsuffix.$fieldprefix."<input id='$id-$field_name' type='number' step=1 name='$field_name'$required$value$condition/>".$fieldsuffix;
        if (isset($field_desc->hint)) $f_proto.="<div class=\"hint\" data-name=\"$field_name\">".$field_desc->hint."</div>";
        $f_proto.=$suffix.PHP_EOL;
        return $f_proto;
      }

      private function form_generator_helper_make_float ($id, $field_name, $field_desc, $prefix, $suffix, $required, $bind, $condition, $default, $value, $f_proto) {
        $labelprefix = "";
        $labelsuffix = "";
        $fieldprefix = "";
        $fieldsuffix = "";
        if (@$field_desc->appearance=="box") {
          $labelprefix = "<div class=\"label_box\">";
          $labelsuffix = "</div>";
          $fieldprefix = "<div class=\"input_box\">";
          $fieldsuffix = "</div>";
        }
        $f_proto.=$prefix;
        $f_proto.=$labelprefix."<label for='$id-$field_name'$condition>".$field_desc->label.
        "</label>".$labelsuffix.$fieldprefix."<input id='$id-$field_name' type='number' step='0.01' name='$field_name'$required$value$condition/>".$fieldsuffix;
        if (isset($field_desc->hint)) $f_proto.="<div class=\"hint\" data-name=\"$field_name\">".$field_desc->hint."</div>";
        $f_proto.=$suffix.PHP_EOL;
        return $f_proto;
      }

      private function form_generator_helper_make_date ($id, $field_name, $field_desc, $prefix, $suffix, $required, $bind, $condition, $default, $value, $f_proto) {
        $labelprefix = "";
        $labelsuffix = "";
        $fieldprefix = "";
        $fieldsuffix = "";
        if (@$field_desc->appearance=="box") {
          $labelprefix = "<div class=\"label_box date\">";
          $labelsuffix = "</div>";
          $fieldprefix = "<div class=\"input_box date\">";
          $fieldsuffix = "</div>";
        }
        $f_proto.=$prefix;
        $f_proto.=$labelprefix."<label for='$id-$field_name'$condition>".$field_desc->label.
        "</label>".$labelsuffix.$fieldprefix."<input id='$id-$field_name' type='date' name='$field_name'$required$value$condition/>".$fieldsuffix;
        if (isset($field_desc->hint)) $f_proto.="<div class=\"hint\" data-name=\"$field_name\">".$field_desc->hint."</div>";
        $f_proto.=$suffix.PHP_EOL;
        return $f_proto;
      }

      private function form_generator_helper_make_confirm ($id, $field_name, $field_desc, $prefix, $suffix, $required, $bind, $condition, $default, $value, $f_proto) {
        $labelprefix = "";
        $labelsuffix = "";
        $fieldprefix = "";
        $fieldsuffix = "";
        if (@$field_desc->appearance=="box") {
          $labelprefix = "<div class=\"label_box\">";
          $labelsuffix = "</div>";
          $fieldprefix = "<div class=\"input_box\">";
          $fieldsuffix = "</div>";
        }
        $f_proto.=$prefix;
        if (@$field_desc->appearance=="blockreverse") {
          $f_proto.=$fieldprefix."<input id='$id-$field_name' type='checkbox' value='confirm' name='$field_name'$required$condition/>".$fieldsuffix.$labelprefix."<label for='$id-$field_name'$condition>".$field_desc->label.
          "</label>".$labelsuffix;
        }
        else {
          $f_proto.=$labelprefix."<label for='$id-$field_name'$condition>".$field_desc->label.
          "</label>".$labelsuffix.$fieldprefix."<input id='$id-$field_name' type='checkbox' value='confirm' name='$field_name'$required$condition/>".$fieldsuffix;
        }
        if (isset($field_desc->hint)) $f_proto.="<div class=\"hint\" data-name=\"$field_name\">".$field_desc->hint."</div>";
        $f_proto.=$suffix.PHP_EOL;
        return $f_proto;
      }

      private function form_generator_helper_make_file ($id, $field_name, $field_desc, $prefix, $suffix, $required, $bind, $condition, $default, $value, $f_proto, $write) {
        $labelprefix = "";
        $labelsuffix = "";
        $fieldprefix = "";
        $fieldsuffix = "";
        if (@$field_desc->appearance=="box") {
          $labelprefix = "<div class=\"label_box\">";
          $labelsuffix = "</div>";
          $fieldprefix = "<div class=\"input_box\">";
          $fieldsuffix = "</div>";
        }
        $show = "visible";
        if (empty($write)) {
          $show = "hidden";
        }
        $f_proto.=$prefix;
        $f_proto.=$labelprefix."<label for='$id-$field_name'$condition>".$field_desc->label.
        "</label>".$labelsuffix.$fieldprefix."<input id='$id-$field_name' data-show='$show' type='file' name='$field_name'$required$condition$default/><span id='$id-$field_name-addendum'></span>".$fieldsuffix;
        if (isset($field_desc->hint)) $f_proto.="<div class=\"hint\" data-name=\"$field_name\">".$field_desc->hint."</div>";
        $f_proto.=$suffix.PHP_EOL;
        return $f_proto;
      }

      private function form_generator_helper_make_color ($id, $field_name, $field_desc, $prefix, $suffix, $required, $bind, $condition, $default, $value, $f_proto, $write) {
        $labelprefix = "";
        $labelsuffix = "";
        $fieldprefix = "";
        $fieldsuffix = "";
        if (@$field_desc->appearance=="box") {
          $labelprefix = "<div class=\"label_box color\">";
          $labelsuffix = "</div>";
          $fieldprefix = "<div class=\"input_box color\">";
          $fieldsuffix = "</div>";
        }
        $f_proto.=$prefix;
        $f_proto.=$labelprefix."<label for='$id-$field_name'$condition>".$field_desc->label.
        "</label>".$labelsuffix.$fieldprefix."<input id='$id-$field_name' type='color' name='$field_name'$required$value$condition/>".$fieldsuffix;
        if (isset($field_desc->hint)) $f_proto.="<div class=\"hint\" data-name=\"$field_name\">".$field_desc->hint."</div>";
        $f_proto.=$suffix.PHP_EOL;
        return $f_proto;
      }

      /**
       * Make form
       * @param {string} form_name
       * @param {object} desc
       */
      public function make_form ($form_name, $desc) {
          $autobind_ar = $this->form_generator_helper_get_autobind_ar($desc->autobind);
          $id = "u-" . $form_name . "-" . $this->serial;
          $read = "";
          $write = "";
          $create = "";
          $delete = "";
          $await = $this->form_generator_helper_process_await($desc);
          $feedback = $this->form_generator_helper_process_feedback($desc);
          if (isset($desc->service_read)) $read = " data-service-read='".$desc->service_read."'";
          if (isset($desc->service_write)) $write = " data-service-write='".$desc->service_write."'";
          if (isset($desc->service_create)) $create = " data-service-create='".$desc->service_create."'";
          if (isset($desc->service_delete)) $delete = " data-service-delete='".$desc->service_delete."'";
          $f_proto = "<form class='autoform' name='$form_name'$read$write$create$delete$feedback$await id='$id'>".PHP_EOL;
          foreach($desc->pages as $page) {
            foreach($page->fields as $field) {
              foreach($autobind_ar as $autobind) {
                $textfields = (array) @$this->models->{$autobind}->{'text_fields'};
                $geofields = (array) @$this->models->{$autobind}->{'geo_fields'};
                $intfields = (array) @$this->models->{$autobind}->{'integer_fields'};
                $floatfields = (array) @$this->models->{$autobind}->{'float_fields'};
                $datefields = (array) @$this->models->{$autobind}->{'date_fields'};
                $confirmfields = (array) @$this->models->{$autobind}->{'confirm_fields'};
                $selectfields = (array) @$this->models->{$autobind}->{'select_fields'};
                $filefields = (array) @$this->models->{$autobind}->{'file_fields'};
                $colorfields = (array) @$this->models->{$autobind}->{'color_fields'};
                if (isset($field->name)) {
                  $field_name = $field->name;
                  $field_desc = $field;
                  $prefix = $this->form_generator_helper_make_prefix($field_name, $field_desc);
                  $suffix = $this->form_generator_helper_make_suffix($field_name, $field_desc);
                  $labelprefix = "";
                  $labelsuffix = "";
                  $fieldprefix = "";
                  $fieldsuffix = "";
                  $required = $this->form_generator_helper_make_extra($field_desc);
                  $value = "";
                  $default = "";
                  if (isset($field_desc->{'default'})) {
                    $value = " "."value=\"".$field_desc->{'default'}."\"";
                    $default = " "."data-default=\"".$field_desc->{'default'}."\"";
                  }
                  $bind = "";
                  if (isset($field_desc->{'bind'})) {
                    $bind = " "."data-bind=\"".$field_desc->{'bind'}."\"";
                  }
                  $condition = "";
                  if (isset($field_desc->{'condition'})) {
                    $condition = " "."data-if=\"".$field_desc->{'condition'}."\"";
                  }
                  if (in_array($field_name, $selectfields) || (!empty($bind) && in_array($field_name, $geofields))) {
                    $f_proto = $this->form_generator_helper_make_select($id, $field_name, $field_desc, $prefix, $suffix, $required, $bind, $condition, $default, $value, $f_proto);
                  }
                  else if (in_array($field_name, $textfields)) {
                    $f_proto = $this->form_generator_helper_make_text($id, $field_name, $field_desc, $prefix, $suffix, $required, $bind, $condition, $default, $value, $f_proto);
                  }
                  else if (in_array($field_name, $geofields) && empty($bind)) {
                    $f_proto = $this->form_generator_helper_make_geo($id, $field_name, $field_desc, $prefix, $suffix, $required, $bind, $condition, $default, $value, $f_proto);
                  }
                  else if (in_array($field_name, $intfields)) {
                    $f_proto = $this->form_generator_helper_make_int($id, $field_name, $field_desc, $prefix, $suffix, $required, $bind, $condition, $default, $value, $f_proto);
                  }
                  else if (in_array($field_name, $floatfields)) {
                    $f_proto = $this->form_generator_helper_make_float($id, $field_name, $field_desc, $prefix, $suffix, $required, $bind, $condition, $default, $value, $f_proto);
                  }
                  else if (in_array($field_name, $datefields)) {
                    $f_proto = $this->form_generator_helper_make_date($id, $field_name, $field_desc, $prefix, $suffix, $required, $bind, $condition, $default, $value, $f_proto);
                  }
                  else if (in_array($field_name, $confirmfields)) {
                    $f_proto = $this->form_generator_helper_make_confirm($id, $field_name, $field_desc, $prefix, $suffix, $required, $bind, $condition, $default, $value, $f_proto);
                  }
                  else if (in_array($field_name, $filefields)) {
                    $f_proto = $this->form_generator_helper_make_file($id, $field_name, $field_desc, $prefix, $suffix, $required, $bind, $condition, $default, $value, $f_proto, $write);
                  }
                  else if (in_array($field_name, $colorfields)) {
                    $f_proto = $this->form_generator_helper_make_color($id, $field_name, $field_desc, $prefix, $suffix, $required, $bind, $condition, $default, $value, $f_proto, $write);
                  }
                }
              }
              if (!isset($field->name)) {
                $field_desc = $field;
                $prefix = "";
                $suffix = "";
                $labelprefix = "";
                $labelsuffix = "";
                if (@$field_desc->appearance=="block" || @$field_desc->appearance=="blockreverse") {
                  $prefix = "<div class='inputwrapper'>";
                  $suffix = "</div>";
                }
                else if (@$field_desc->appearance=="startblock") {
                  $prefix = "<div class='inputblock'><div class='inputwrapper'>";
                  $suffix = "</div>";
                }
                else if (@$field_desc->appearance=="endblock") {
                  $prefix = "<div class='inputwrapper'>";
                  $suffix = "</div></div>";
                }
                if (@$field_desc->appearance=="box") {
                  $labelprefix = "<div class=\"label_box\">";
                  $labelsuffix = "</div>";
                }
                $condition = "";
                if (isset($field_desc->{'condition'})) {
                  $condition = " "."data-if=\"".$field_desc->{'condition'}."\"";
                }
                $f_proto.=$prefix.$labelprefix."<label$condition>".$field_desc->label."</label>".$labelsuffix.$suffix.PHP_EOL;
              }
            }
          }
          $f_proto.="<input id='$id-id' type='hidden' name='id'/>".PHP_EOL;
          $f_proto.="<input id='$id-parent_id' type='hidden' name='parent_id'/>".PHP_EOL;
          foreach($page->triggers as $field_name=>$field_desc) {
            $condition = "";
            if (isset($field_desc->{'condition'})) {
              $condition = " "."data-if=\"".$field_desc->{'condition'}."\"";
            }
            if (@$field_desc->{'appearance'} === "box") {
              $f_proto.="<div class=\"trigger-type trigger-type-".$field_name."\">";
            }
            switch ($field_name) {
              case "submit": $f_proto.= "<input type='submit' id='$id-submit' value=\"".@$field_desc->label."\"$condition/>"; break;
              case "submit-novalidate": $f_proto.= "<input type='submit' id='$id-submit-novalidate' value=\"".@$field_desc->label."\" formnovalidate$condition/>"; break;
              case "reset": $f_proto.= "<input type='reset' id='$id-reset' value=\"".@$field_desc->label."\"$condition/>"; break;
              case "create": $f_proto.= "<input type='button' id='$id-create' data-name='create' value=\"".@$field_desc->label."\"$condition/>"; break;
              case "duplicate": $f_proto.= "<input type='button' id='$id-duplicate' data-name='duplicate' value=\"".@$field_desc->label."\"$condition/>"; break;
              case "delete": $f_proto.= "<input type='button' id='$id-delete' data-name='delete' value=\"".@$field_desc->label."\"$condition/>"; break;
              default: {
                $f_proto.= "<span class='autoform-trigger' data-name='$field_name' id='$id-$field_name' data-service='".
                @$field_desc->service."'$condition>".$field_desc->label."</span>";
              } break;
            }
            if (@$field_desc->{'appearance'} === "box") {
              $f_proto.="</div>";
            }
          }
          $f_proto.= "</form>".PHP_EOL;
          file_put_contents("dist".DIRECTORY_SEPARATOR."templates".DIRECTORY_SEPARATOR."form_".$form_name.".html.tpl", $f_proto);
      }
  }
?>
