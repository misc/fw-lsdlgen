<?php
    namespace LSDL\protogen\lib;

    /**
     * Trait ServiceGenerator
     * @author  Martin Springwald <martin.springwald@informatik.tu-chemnitz.de>
     * @license     Greenscale Open Source License
     */
    trait ServiceGenerator {
        /**
         * Begin Service controller
         * @param {string} ucprefix
         * @param {string} shortprefix
         * @return
         */
        private function prepare_service_controller ($ucprefix, $shortprefix) {
            $uccontroller = $ucprefix;
            $c_proto = file_get_contents(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."prototypes".DIRECTORY_SEPARATOR."controller.prototype.php");
            $c_proto = str_replace("%Controller%", $uccontroller, $c_proto);
            $c_proto = str_replace("%Prefix%", $ucprefix, $c_proto);
            $c_proto = str_replace("%prefix%", $shortprefix, $c_proto);
            $c_proto = str_replace("%vendor%", $this->vendor, $c_proto);
            return $c_proto;
        }

        /**
         * Create setup script
         * @param {string} dbrequires
         * @param {string} dbuses
         * @param {string} dbinstances
         * @param {string} dbcreates
         */
        public function make_setup ($dbrequires, $dbuses, $dbinstances, $dbcreates, $dbclears, $dbdrops, $dbviewcreates, $dbviewclears, $dbviewdrops) {
            $dbuses.="\tuse greenscale\server\database\DatabaseTable;".PHP_EOL;
            $setup_instances = str_replace("\t\t", "", str_replace("\$this->", "\$", $dbinstances)).PHP_EOL;
            $d_proto = file_get_contents(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."prototypes".DIRECTORY_SEPARATOR."setup-database.prototype.php");
            $d_proto = str_replace("%prefix%", $this->prefix, $d_proto);
            $d_proto = str_replace("%dbuses%", $dbuses, $d_proto);
            $d_proto = str_replace("%dbinstances%", $setup_instances, $d_proto);
            $d_proto = str_replace("%dbcreates%", $dbcreates, $d_proto);
            $d_proto = str_replace("%dbclears%", $dbclears, $d_proto);
            $d_proto = str_replace("%dbdrops%", $dbdrops, $d_proto);
            $d_proto = str_replace("%dbviewcreates%", $dbviewcreates, $d_proto);
            $d_proto = str_replace("%dbviewclears%", $dbviewclears, $d_proto);
            $d_proto = str_replace("%dbviewdrops%", $dbviewdrops, $d_proto);
            file_put_contents(
                "dist".DIRECTORY_SEPARATOR."setup".DIRECTORY_SEPARATOR."setup-".$this->prefix."-database.php",
                $d_proto
            );
            $a_proto = file_get_contents(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."prototypes".DIRECTORY_SEPARATOR."setup-app.prototype.php");
            $a_proto = str_replace("%prefix%", $this->prefix, $a_proto);
            file_put_contents(
                "dist".DIRECTORY_SEPARATOR."setup".DIRECTORY_SEPARATOR."setup-".$this->prefix."-app.php",
                $a_proto
            );
        }

        /**
         * Support database usage in controller
         * @param {string} &c_proto
         */
        public function init_dbs_in_service (&$c_proto) {
            $dbvars = "";
            $dbinstances = "";
            $dbuses = "";
            $dbrequires = "";
            $dbcreates = "";
            $dbdrops = "";
            $dbclears = "";
            $dbviewcreates = "";
            $dbviewdrops = "";
            $dbviewclears = "";
            foreach(glob("plugins/*.php") as $plugin) {
                $dbrequires.="    require_once('plugins/".basename($plugin)."');".PHP_EOL;
            }
            foreach($this->databases as $database) {
                $ucdomstr = $this->get_ucdomstr($database);
                $dbvars.=<<<EOT
        /**
         * Database instance
         * @var \Database
         */
        private \$db_$database = null;

EOT;
                $dbinstances.="            \$this->db_$database = $ucdomstr"."Database::get_instance();".PHP_EOL;
                $dbuses.="    use ".$this->vendor."\\server\\database\\".$ucdomstr."Database;".PHP_EOL;
                // $dbrequires.="\trequire_once('database/db.".$database.".php');".PHP_EOL;
                if (substr_count($database, "_") > 1) {
                    $dbviewcreates.="try { \t\$db_$database"."->createScheme(); } catch(Exception \$e) {echo \"$database\".\": \".\$e->getMessage().PHP_EOL;}".PHP_EOL;
                    $dbviewdrops.="try { \t@\$db_$database"."->query_spread(\$db_$database"."->preprocess_template(\"%FKOFF%;\")); } catch (Exception \$e) {echo \"$database\".\": \".\$e->getMessage().PHP_EOL;}".PHP_EOL;
                    $dbviewdrops.="try { \t@\$db_$database"."->dropTable(); } catch (Exception \$e) {echo \"$database\".\": \".\$e->getMessage().PHP_EOL;}".PHP_EOL;
                    $dbviewdrops.="try { \t@\$db_$database"."->query_spread(\$db_$database"."->preprocess_template(\"%FKON%;\")); } catch (Exception \$e) {echo \"$database\".\": \".\$e->getMessage().PHP_EOL;}".PHP_EOL;
                    $dbviewclears.="try { \t@\$db_$database"."->clearTable(); } catch (Exception \$e) {echo \"$database\".\": \".\$e->getMessage().PHP_EOL;}".PHP_EOL;
                }
                else {
                    $dbcreates.="try { \t\$db_$database"."->createScheme(); } catch(Exception \$e) {echo \"$database\".\": \".\$e->getMessage().PHP_EOL;}".PHP_EOL;
                    $dbdrops.="try { \t@\$db_$database"."->query_spread(\$db_$database"."->preprocess_template(\"%FKOFF%;\")); } catch (Exception \$e) {echo \"$database\".\": \".\$e->getMessage().PHP_EOL;}".PHP_EOL;
                    $dbdrops.="try { \t@\$db_$database"."->dropTable(); } catch (Exception \$e) {echo \"$database\".\": \".\$e->getMessage().PHP_EOL;}".PHP_EOL;
                    $dbdrops.="try { \t@\$db_$database"."->query_spread(\$db_$database"."->preprocess_template(\"%FKON%;\")); } catch (Exception \$e) {echo \"$database\".\": \".\$e->getMessage().PHP_EOL;}".PHP_EOL;
                    $dbclears.="try { \t@\$db_$database"."->clearTable(); } catch (Exception \$e) {echo \"$database\".\": \".\$e->getMessage().PHP_EOL;}".PHP_EOL;
                }
            }
            $c_proto = str_replace("%dbvars%", $dbvars, $c_proto);
            $c_proto = str_replace("%dbinstances%", $dbinstances, $c_proto);
            $c_proto = str_replace("%dbuses%", $dbuses, $c_proto);
            $c_proto = str_replace("%dbrequires%", $dbrequires, $c_proto);
            $this->make_setup($dbrequires, $dbuses, $dbinstances, $dbcreates, $dbclears, $dbdrops, $dbviewcreates, $dbviewclears, $dbviewdrops);
        }
        
        /**
         * Replace SQL parameters
         * @param {string} $parameter
         * @return
         */
        private function replace_sql_parameters ($domain, $parameter) {
            $sql_substitues_source = [
                "%NOW%",
                "%AGGREGATE%",
                "%SEPARATOR%",
                "%CASE%",
                "%THEN%",
                "%ELSE%",
                "%END%",
                "%SEL%"
            ];
            $sql_substitutes_target = [
                '{$'."this->db_".$domain."->preprocess_template(\"%NOW%\")}",
                '{$'."this->db_".$domain."->preprocess_template(\"%AGGREGATE%\")}",
                '{$'."this->db_".$domain."->preprocess_template(\"%SEPARATOR%\")}",
                '{$'."this->db_".$domain."->preprocess_template(\"%CASE%\")}",
                '{$'."this->db_".$domain."->preprocess_template(\"%THEN%\")}",
                '{$'."this->db_".$domain."->preprocess_template(\"%ELSE%\")}",
                '{$'."this->db_".$domain."->preprocess_template(\"%END%\")}",
                '{$'."this->db_".$domain."->preprocess_template(\"%SEL%\")}"
            ];
            return str_replace($sql_substitues_source, $sql_substitutes_target, $parameter);
        }

        /**
         * Process service parameters
         * @param {object} desc
         * @param {string} &params
         */
        public function process_service_parameters ($desc, &$params) {
            $delim = "";
            if (isset($desc->return->parameters)) {
                foreach($desc->return->parameters as $parameter) {
                    if (strpos($parameter, "{")===0) {
                        $obj = (array) json_decode($parameter);
                        $parameter = str_replace(PHP_EOL, "", var_export($obj, true));
                        $params .= $delim . "JSON::".base64_encode(str_replace("'","\"", str_replace(".", "->", $parameter)));
                    }
                    else if (strpos($parameter, "[")===0) {
                        $params .= $delim . $this->replace_sql_parameters($desc->return->domain, str_replace(".", "->", $parameter));
                    }
                    else {
                        $params .= $delim . '$'.str_replace(".", "->", $parameter);
                    }
                    $delim = ", ";
                }
            }
        }

        /**
         * Process JSON
         * @param {string} $params
         * @return {array}
         */
        private function process_json ($params) {
          $split_params = explode(", ", $params);
          foreach ($split_params as $key => $param) {
            if (strpos($param, "JSON::")===0) {
                $split_params[$key] = base64_decode(substr($param, 6));
            }
          }
          return $split_params;
        }

        /**
         * Make service actions
         * @param {object} value
         */
        public function make_service_actions ($value) {
            $actions = "";
            foreach($value as $service=>$desc) {
                $actions.="                    case \"$service\": {".PHP_EOL;
                $params = "";
                $this->process_service_parameters($desc, $params);
                if ($desc->return->method === "upsert") {
                    $split_params = $this->process_json($params);
                    $relation = @$desc->return->relation;
                    $relation_desc = @$this->models->{$relation};
                    if ($relation_desc != null) $relation_desc = $relation_desc->type;
                    switch ($relation_desc) {
                        case "1:1": {
                            $first = explode("_", $relation)[0];
                            $second = explode("_", $relation)[1];
                            $param_a = $split_params[0];
                            $param_b = $split_params[1];
                            $domain = @$desc->return->domain;
                            if ($domain == null) {
                                $param_c = $split_params[2];
                                $actions.=<<<EOT
                        \$result = \$this->db_{$relation}->filter((array) array('{$first}_id'=>$param_a, '{$second}_id'=>$param_b));
                        if (\$result != null && count(\$result)>0) {
                            \$rel_id = \$result[0]->id;
                            \$update_result = \$this->db_{$relation}->update(\$rel_id, (array) $param_c);
                            if (\$update_result > 0) {
                                \$result = \$rel_id;
                            }
                            else {
                                \$result = \$rel_id;
                            }
                        }
                        else {
                            \$rel_id = \$this->db_{$relation}->insert(array_merge((array) array('{$first}_id'=>$param_a, '{$second}_id'=>\${$second}_id), (array) $param_c));
                            \$result = \$rel_id;
                        }
EOT;
                            }
                            else {
                                $actions.=<<<EOT
                        \$result = \$this->db_{$relation}->filter((array) array('{$first}_id'=>$param_a));
                        if (\$result != null && count(\$result)>0) {
                            \${$second}_id = \$result[0]->{$second}_id;
                            \$update_result = \$this->db_{$domain}->update(\${$second}_id, (array) $param_b);
                            if (\$update_result > 0) {
                                \$result = \${$second}_id;
                            }
                            else {
                                \$result = \${$second}_id;
                            }
                        }
                        else {
                            \${$second}_id = \$this->db_{$domain}->insert((array) $param_b);
                            \$this->db_{$relation}->insert((array) array('{$first}_id'=>$param_a, '{$second}_id'=>\${$second}_id));
                            \$result = \${$second}_id;
                        }
EOT;
                            }
                            $actions.=PHP_EOL;
                        } break;
                        case "1:n":
                        case "n:m": {
                            $first = explode("_", $relation)[0];
                            $second = explode("_", $relation)[1];
                            $param_a = $split_params[0];
                            $param_b = $split_params[1];
                            $param_c = $split_params[2];
                            $domain = @$desc->return->domain;
                            if ($domain == null) {
                              $actions.=<<<EOT
                        \$result = \$this->db_{$relation}->filter((array) array('{$first}_id'=>$param_a, '{$second}_id'=>$param_b));
                        if (\$result != null && count(\$result)>0) {
                            \$rel_id = \$result[0]->id;
                            \$update_result = \$this->db_{$relation}->update(\$rel_id, (array) $param_c);
                            if (\$update_result > 0) {
                                \$result = \$rel_id;
                            }
                            else {
                                \$result = \$rel_id;
                            }
                        }
                        else {
                            \$rel_id = \$this->db_{$relation}->insert(array_merge((array) array('{$first}_id'=>$param_a, '{$second}_id'=>$param_b), (array) $param_c));
                            \$result = \$rel_id;
                        }
EOT;
                            }
                            else {
                              $actions.=<<<EOT
                        \$result = \$this->db_{$relation}->filter((array) array('{$first}_id'=>$param_a, '{$second}_id'=>$param_b));
                        if (\$result != null && count(\$result)>0) {
                            \$rel_id = \$result[0]->id;
                            \$update_result = \$this->db_{$domain}->update($param_b, (array) $param_c);
                            if (\$update_result > 0) {
                                \$result = \$rel_id;
                            }
                            else {
                                \$result = \$rel_id;
                            }
                        }
                        else {
                            \${$domain}_id = \$this->db_{$domain}->insert((array) $param_c);
                            \$relation_id = \$this->db_{$relation}->insert((array) array('{$first}_id'=>$param_a, '{$second}_id'=>\${$second}_id));
                            \$this->db_{$relation}->update(\$relation_id, (array) $param_c);
                            \$result = \${$domain}_id;
                        }
EOT;
                            }
                            $actions.=PHP_EOL;
                        } break;
                        default: {
                            $param_a = $split_params[0];
                            $param_b = $split_params[1];
                            $domain = @$desc->return->domain;
                            if ($domain != null) {
                                $actions.=<<<EOT
                        \$result = \$this->db_{$domain}->filter((array) array('id'=>$param_a));
                        if (\$result != null && count(\$result)>0) {
                            \${$domain}_id = \$result[0]->id;
                            \$update_result = \$this->db_{$domain}->update(\${$domain}_id, (array) $param_b);
                            if (\$update_result > 0) {
                                \$result = \${$domain}_id;
                            }
                            else {
                                \$result = \${$domain}_id;
                            }
                        }
                        else {
                            \$result = \$this->db_{$domain}->insert((array) $param_b);
                        }
EOT;
                            }
                            $actions.=PHP_EOL;
                        } break;
                    }
                }
                else if ($desc->return->method === "readall") {
                    $split_params = explode(", ", $params);
                    $relation = $desc->return->relation;
                    $first = explode("_", $relation)[0];
                    $second = explode("_", $relation)[1];
                    $param_a = $split_params[0];
                    $domain = $desc->return->domain;
                    $actions.=<<<EOT
                        \$result = \$this->db_{$relation}->filter((array) array('{$first}_id'=>$param_a));
                        if (\$result != null && count(\$result)>0) {
                            \${$second}_id = \$result[0]->{$second}_id;
                            \$result = \$this->db_{$domain}->read(\${$second}_id);
                        }
                        else {
                            \$result = null;
                        }
EOT;
                    $actions.=PHP_EOL;
                }
                else {
                    $domain = $desc->return->domain;
                    $method = $desc->return->method;
                    $params = implode(', ', $this->process_json($params));
                    $actions.=<<<EOT
                        \$result = \$this->db_$domain->$method($params);

EOT;
                }
                if (isset($desc->return->translate)) {
                    $field_list = json_encode($desc->return->translate);
                    $actions.=<<<EOT
                        \$this->translateResult(\$result, [$field_list]);

EOT;
                }
                if (isset($desc->return->transform)) {
                    $actions.=<<<EOT
                        {$desc->return->transform}(\$result, \$user);

EOT;
                }
                $actions.=<<<EOT
                        if (\$plain_result === true) {
                            return \$result;
                        }
                        else {
                            return new OutputSuccess(\$this->allow, \$result);
                        }
                        break;
                    }

EOT;
            }
            foreach($this->processes as $process=>$desc) {
                $actions.="                    case \"$process\": {".PHP_EOL;
                if (isset($desc->chain)) {
                    foreach($desc->chain as $action) {
                        //if (empty(\$result) && \$result !== 0 && \$result !== "0" && \$result !== 0.0) throw new UserAdminException("FailedAction:$action");
                        $actions.=<<<EOT
                        \$data->action = "$action";
                        \$result = \$this->processPostRequest(\$data, true);
                        if (\$result === "" || \$result === null || \$result === false) throw new UserAdminException("FailedAction:$action");
                        \$data->result = \$result;
EOT;
                    $actions.=PHP_EOL;
                    }
                }
                $actions.=<<<EOT
                        return new OutputSuccess(\$this->allow, \$result);
                        break;
                    }

EOT;
            }
            return $actions;
        }

        /**
         * Make controller
         * @param {object} value
         */
        public function make_controller ($value) {
            // server side
            $shortprefix = str_replace("_", "", $this->prefix);
            $ucprefix = ucfirst($shortprefix);
            $c_proto = $this->prepare_service_controller($ucprefix, $shortprefix);
            $this->init_dbs_in_service($c_proto);
            $c_proto = str_replace("%actions%", $this->make_service_actions($value), $c_proto);
            file_put_contents("dist".DIRECTORY_SEPARATOR.$this->prefix.DIRECTORY_SEPARATOR."$ucprefix.controller.php", $c_proto);
            // client side
            $j_proto = file_get_contents(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."prototypes".DIRECTORY_SEPARATOR."client.prototype.js");
            $j_proto = str_replace("%prefix%", $this->prefix, $j_proto);
            $j_proto = str_replace("%apiurl%", "index.php".DIRECTORY_SEPARATOR.$this->prefix."_api", $j_proto);
            file_put_contents("dist".DIRECTORY_SEPARATOR."public".DIRECTORY_SEPARATOR."client.js", $j_proto);
            // index
            $uses = "";
            $installs = "";
            $requires = "";
            foreach($this->pages as $page) {
                $ucpage = ucfirst($page);
                $installs.="\tnew $ucpage"."Page(\$router);".PHP_EOL;
                $uses.="\tuse ".$this->vendor."\\server\\".$this->prefix."\\".$ucpage."Page;".PHP_EOL;
                $requires.="\trequire_once('".$this->prefix."/$ucpage.page.php');".PHP_EOL;
            }
            $installs.="\tnew $ucprefix"."Controller(\$router);".PHP_EOL;
            $uses.="\tuse ".$this->vendor."\\server\\".$this->prefix."\\".$ucprefix."Controller;".PHP_EOL;
            $requires.="\trequire_once('".$this->prefix."/$ucprefix.controller.php');".PHP_EOL;
            $i_proto = file_get_contents(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."prototypes".DIRECTORY_SEPARATOR."index.prototype.php");
            $i_proto = str_replace("%vendor%", $this->vendor, $i_proto);
            $i_proto = str_replace("%uses%", $uses, $i_proto);
            $i_proto = str_replace("%installs%", $installs, $i_proto);
            $i_proto = str_replace("%requires%", $requires, $i_proto);
            file_put_contents("dist".DIRECTORY_SEPARATOR."public".DIRECTORY_SEPARATOR."index.php", $i_proto);
            $p_proto = file_get_contents(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."prototypes".DIRECTORY_SEPARATOR."composer.prototype.json");
            $p_proto = str_replace("%vendor%", $this->vendor, $p_proto);
            $p_proto = str_replace("%prefix%", $this->prefix, $p_proto);
            $bins = "";
            $bins_delim = "";
            foreach(glob("cron/*.php") as $plugin) {
                $bins.=$bins_delim."\t\t".'"'.'cron/'.basename($plugin).'",';
                $bins_delim = PHP_EOL;
            }
            $p_proto = str_replace("%bins%", $bins, $p_proto);
            file_put_contents("dist".DIRECTORY_SEPARATOR."composer.json", $p_proto);
        }
    }
?>
