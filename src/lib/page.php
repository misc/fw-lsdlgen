<?php
  namespace LSDL\protogen\lib;

  /**
   * Trait PageGenerator
   * @author  Martin Springwald <martin.springwald@informatik.tu-chemnitz.de>
   * @license   Greenscale Open Source License
   */
  trait PageGenerator {
    /**
     * Make page
     * @param {string} page
     * @param {object} desc
     */
    public function make_page ($page, $desc) {
      $requires = "";
      foreach(glob("plugins/*.php") as $plugin) {
        $requires.="    require_once('plugins/".basename($plugin)."');".PHP_EOL;
      }
      $p_proto = file_get_contents(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."prototypes".DIRECTORY_SEPARATOR."page.prototype.php");
      $p_proto = str_replace("%Page%", ucfirst($page), $p_proto);
      $p_proto = str_replace("%page%", $page, $p_proto);
      $p_proto = str_replace("%requires%", $requires, $p_proto);
      $p_proto = str_replace("%prefix%", str_replace("_", "", $this->prefix), $p_proto);
      $p_proto = str_replace("%vendor%", $this->vendor, $p_proto);
      $p_proto = str_replace("%commentcache%", (isset($desc->cache) && ($desc->cache === "disable")) ? "//" : "", $p_proto);
      file_put_contents("dist".DIRECTORY_SEPARATOR.$this->prefix.DIRECTORY_SEPARATOR.ucfirst($page).".page.php", $p_proto);
    }
  }
?>
