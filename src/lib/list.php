<?php
    namespace LSDL\protogen\lib;

    /**
     * Trait ListGenerator
     * @author  Martin Springwald <martin.springwald@informatik.tu-chemnitz.de>
     * @license     Greenscale Open Source License
     */
    trait ListGenerator {
        /**
         * Make list
         * @param {string} list_name
         * @param {object} desc
         */
        public function make_list ($list_name, $desc) {
            $autobind = $desc->autobind;
            $addendum = "";
            if (isset($desc->linkform)) {
                $addendum.= " data-link-form='".$desc->linkform."'";
            }
            if (isset($desc->linklist)) {
                $addendum.= " data-link-list='".$desc->linklist."'";
            }
            if (isset($desc->delegate)) {
                $addendum.= " data-delegate-target='".$desc->delegate."'";
            }
            if (isset($desc->await)) {
                $addendum.= " data-await='".$desc->await."'";
            }
            $is_multiselect = false;
            $trigger_classes = "autolist-trigger";
            $id = "u-" . $list_name . "-" . $this->serial;
            $f_proto = "<div id='$id-search' class='search'><input id='$id-searchfield' class='searchfield'/></div>".PHP_EOL;
            $f_proto.= "<div id='$id-filter' class='filter'><select id='$id-filterfield' class='filterfield'>".PHP_EOL;
            $f_proto.= "<option value=''>--</option>".PHP_EOL;
            if (isset($desc->filter)) {
              foreach($desc->filter as $filter) {
          $f_proto.="<option value='".$filter->condition."'>".$filter->title."</option>".PHP_EOL;
              }
            }
            $f_proto.="</select></div>".PHP_EOL;
            $f_proto.= "<div id='$id-export' class='export'></div>".PHP_EOL;
            foreach($desc->pages as $page) {
              if (isset($page->triggers)) {
                  foreach($page->triggers as $field_name=>$field_desc) {
                    if (isset($field_desc->mode) && $field_desc->mode==="multiselect") {
                      $f_proto.= "<span class='".$trigger_classes."' data-name='$field_name' data-service='".
                      $field_desc->service."' data-mode='multiselect' data-for-id='$id'>".$field_desc->label."</span>";
                      $is_multiselect = true;
                    }
                  }
              }
            }
            $f_proto.= "<ul class='autolist' data-name='$list_name' data-service-list='".$desc->service_list."'$addendum id='$id'></ul>".PHP_EOL;
            $f_proto.= "<template id='autolist-$list_name'>".PHP_EOL;
            $f_proto.= "<li>";
            foreach($desc->pages as $page) {
        if ($is_multiselect) {
          $f_proto.="<input type='checkbox' name='multiselect-$list_name'/>";
        }
                foreach($page->fields as $field) {
                  $field_name = $field->name;
                  $field_desc = $field;
                    $textfields = (array) @$this->models->{$autobind}->{'text_fields'};
                    $geofields = (array) @$this->models->{$autobind}->{'geo_fields'};
                    $intfields = (array) @$this->models->{$autobind}->{'integer_fields'};
                    $floatfields = (array) @$this->models->{$autobind}->{'float_fields'};
                    $datefields = (array) @$this->models->{$autobind}->{'date_fields'};
                    $filefields = (array) @$this->models->{$autobind}->{'file_fields'};
                    $confirmfields = (array) @$this->models->{$autobind}->{'confirm_fields'};
                    $colorfields = (array) @$this->models->{$autobind}->{'color_fields'};
          $class = "";
          $prefix = "";
          $suffix = "";
                    if (@$field_desc->appearance=="block") {
                        $prefix = "<div>";
                    if (isset($field_desc->{'class'})) {
                      $prefix = "<div class=\"".$field_desc->{'class'}."\">";
                    }
                        $suffix = "</div>";
                    }
                    else if (@$field_desc->appearance=="startblock") {
                      $prefix = "<div>";
                    if (isset($field_desc->{'class'})) {
                      $prefix = "<div class=\"".$field_desc->{'class'}."\">";
                    }
                    }
                    else if (@$field_desc->appearance=="endblock") {
                      $suffix = "</div>";
                    }
                    else {
                    if (isset($field_desc->{'class'})) {
                      $class = " class=\"".$field_desc->{'class'}."\"";
                      $trigger_classes .= " ".$field_desc->{'class'};
                    }
                    }
                    if (in_array($field_name, $textfields)) {
                        $f_proto.=$prefix."<label>".$field_desc->label."</label><span data-name='$field_name'$class></span>".$suffix.PHP_EOL;
                    }
                    else if (in_array($field_name, $geofields)) {
                        $f_proto.=$prefix."<label>".$field_desc->label."</label><span data-name='$field_name'$class></span>".$suffix.PHP_EOL;
                    }
                    else if (in_array($field_name, $intfields)) {
                        $f_proto.=$prefix."<label>".$field_desc->label."</label><span data-name='$field_name'$class></span>".$suffix.PHP_EOL;
                    }
                    else if (in_array($field_name, $floatfields)) {
                        $f_proto.=$prefix."<label>".$field_desc->label."</label><span data-name='$field_name'$class></span>".$suffix.PHP_EOL;
                    }
                    else if (in_array($field_name, $datefields)) {
                        $f_proto.=$prefix."<label>".$field_desc->label."</label><span data-name='$field_name'$class></span>".$suffix.PHP_EOL;
                    }
                    else if (in_array($field_name, $filefields)) {
                        $f_proto.=$prefix."<label>".$field_desc->label."</label><span data-identifier data-name='$field_name'$class></span>".$suffix.PHP_EOL;
                    }
                    else if (in_array($field_name, $confirmfields)) {
                        $f_proto.=$prefix."<label>".$field_desc->label."</label><span data-name='$field_name'$class></span>".$suffix.PHP_EOL;
                    }
                    else if (in_array($field_name, $colorfields)) {
                        $f_proto.=$prefix."<label>".$field_desc->label."</label><span data-name='$field_name'$class></span>".$suffix.PHP_EOL;
                    }
                }
                if (isset($page->triggers)) {
                    foreach($page->triggers as $field_name=>$field_desc) {
                        $toggle = "";
                        if (isset($field_desc->toggle)) {
                            $toggle = $field_desc->toggle;
                        }
                        $f_proto.= "<span class='".$trigger_classes."' data-name='$field_name' data-toggle='$toggle' data-service='".
                        @$field_desc->service."'>".$field_desc->label."</span>";
                    }
                }
            }
            $f_proto.= "</li>";
            $f_proto.= "</template>".PHP_EOL;
            file_put_contents("dist".DIRECTORY_SEPARATOR."templates".DIRECTORY_SEPARATOR."list_".$list_name.".html.tpl", $f_proto);
        }
    }
?>
