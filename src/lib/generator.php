<?php
  namespace LSDL\protogen\lib;

  /**
   * Generic Web Framework Code generator
   * Prototype implementation of LSDL research project and leveraging Blobfish framework
   * @author  Martin Springwald <martin.springwald@informatik.tu-chemnitz.de>
   * @license Greenscale Open Source License
   */
  class Generator {
    use DatabaseGenerator, FormGenerator, ListGenerator, PageGenerator, ServiceGenerator, Handler, Helper;

    private $databases = [];
    private $aliases = null;
    private $models = null;
    private $entries = null;
    private $items = null;
    private $pages = [];
    private $processes = [];
    private $datalists = null;

    public $prefix = null;
    public $vendor = null;

    public $serial = null;

    public function __construct () {
      $this->aliases = (object) array();
      $this->datalists = (object) array();
      $this->models = (object) array();
      $this->entries = (object) array();
    }

    private function parse () {
      $this->items = json_decode(file_get_contents("contract.json"));
    }

    private function iterate () {
      foreach($this->items as $key=>$value) {
        switch ($key) {
          case "prefix": $this->entries->prefix = $value; break;
          case "vendor": $this->entries->vendor = $value; break;
          case "serial": $this->entries->serial = $value; break;
          case "datalists": $this->entries->datalists = $value; break;
          case "domains": $this->entries->domains = $value; break;
          case "relations": $this->entries->relations = $value; break;
          case "views": $this->entries->views = $value; break;
          case "forms": $this->entries->forms = $value; break;
          case "lists": $this->entries->lists = $value; break;
          case "pages": $this->entries->pages = $value; break;
          case "processes": $this->entries->processes = $value; break;
          case "services": $this->entries->services = $value; break;
        }
      }
    }

    private function handle () {
      $this->handle_prefix(@$this->entries->prefix);
      $this->handle_vendor(@$this->entries->vendor);
      $this->handle_serial(@$this->entries->serial);
      $this->handle_datalists(@$this->entries->datalists);
      $this->handle_domains(@$this->entries->domains);
      $this->handle_relations(@$this->entries->relations);
      $this->handle_views(@$this->entries->views);
      $this->handle_forms(@$this->entries->forms);
      $this->handle_lists(@$this->entries->lists);
      $this->handle_pages(@$this->entries->pages);
      $this->handle_processes(@$this->entries->processes);
      $this->handle_services(@$this->entries->services);
    }

    public function generate () {
      $this->ensure_queries_dir();
      $this->parse();
      $this->iterate();
      $this->handle();
    }
  }
?>
