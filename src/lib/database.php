<?php
    namespace LSDL\protogen\lib;

    /**
     * Trait Database
     * @author  Martin Springwald <martin.springwald@informatik.tu-chemnitz.de>
     * @license     Greenscale Open Source License
     */
    trait DatabaseGenerator {
        // Map description field types to database field types
        private $SQL_FIELD_MAP = [
            'text_fields'=>' '.SQLTerms::SQL_TEXT_TYPE,
            'geo_fields'=>' '.SQLTerms::SQL_TEXT_TYPE,
            'integer_fields'=>' '.SQLTerms::SQL_INTEGER_TYPE,
            'float_fields'=>' '.SQLTerms::SQL_REAL_TYPE,
            'date_fields'=>' '.SQLTerms::SQL_DATE_TYPE,
            'confirm_fields_0'=>' '.SQLTerms::SQL_INTEGER_TYPE,
            'confirm_fields_1'=>'_confirmed '.SQLTerms::SQL_DATE_TYPE,
            'select_fields'=>' '.SQLTerms::SQL_KEY_TYPE,
            'file_fields'=>' '.SQLTerms::SQL_TEXT_TYPE,
            'color_fields'=>' '.SQLTerms::SQL_TEXT_TYPE
        ];

        /**
         * Add fields to CREATE statement
         * @param {object} desc Field descriptions
         * @param {string} field_type Key in Field descriptions
         * @param {string} db_type Database Field type
         * @param {string} statement Reference to appendable statement string
         */
        private function add_fields ($desc, $field_type, $db_type, &$statement) {
            $field_type = preg_replace("/_[0-9]+$/", "", $field_type);
            if (isset($desc->{$field_type})) {
                foreach ($desc->{$field_type} as $field) {
                    $statement .= ", ".$field . "$db_type";
                }
            }
        }

        /**
         * Add field to CREATE statement
         * @param {string} field Field name
         * @param {string} db_type Database Field type
         * @param {string} statement Reference to appendable statement string
         */
        private function add_field ($field, $db_type, &$statement) {
            $statement .= ", ".$field . " $db_type";
        }

        /**
         * Make SQL CREATE statement
         * @param {string} table_name
         * @param {object} desc
         * @param {string} left_table (optional)
         * @param {string} right_table (optional)
         * @return
         */
        public function make_sql_create_statement ($domain, $table_name, $desc, $left_table = null, $right_table = null) {
            // intro
            $statement = str_replace("%table_name%", $table_name, SQLTerms::SQL_CREATE_INTRO);

            // relation key fields
            if ($left_table != null) $this->add_field($left_table."_id", ' '.SQLTerms::SQL_KEY_TYPE, $statement);
            if ($right_table != null) $this->add_field($right_table."_id", ' '.SQLTerms::SQL_KEY_TYPE, $statement);

            // content fields
            foreach($this->SQL_FIELD_MAP as $field_type => $db_type) $this->add_fields($desc, $field_type, $db_type, $statement);

            // timestamp field
            $this->add_field("last_mod", SQLTerms::SQL_AUTO_TIMESTAMP, $statement);

            // select fields
            if (isset($desc->select_fields)) {
                foreach($this->datalists as $datalist=>$datalist_desc) {
                    if (isset($datalist_desc->{$domain})) {
                        foreach($datalist_desc->{$domain} as $select_field) {
                            if (in_array($select_field, $desc->select_fields)) {
                                $value = null;
                                if (isset($this->aliases->{$datalist})) {
                                    $value = str_replace("%fq_table%", $this->aliases->{$datalist}, ' '.SQLTerms::SQL_REFERENCES);
                                }
                                else {
                                    $value = str_replace("%fq_table%", $this->prefix."_".$datalist, ' '.SQLTerms::SQL_REFERENCES);
                                }
                                $this->add_field(str_replace("%key%", $select_field, SQLTerms::SQL_FOREIGN_KEY), $value, $statement);
                            }
                        }
                    }
                }
            }

            // foreign keys
            if ($left_table != null) {
                $value = null;
                if (isset($this->aliases->{$left_table})) {
                    $value = str_replace("%fq_table%", $this->aliases->{$left_table}, ' '.SQLTerms::SQL_REFERENCES);
                }
                else {
                    $value = str_replace("%fq_table%", $this->prefix."_".$left_table, ' '.SQLTerms::SQL_REFERENCES);
                }
                $this->add_field(str_replace("%key%", $left_table."_id", SQLTerms::SQL_FOREIGN_KEY), $value, $statement);
            }
            if ($right_table != null) {
                $value = null;
                if (isset($this->aliases->{$right_table})) {
                    $value = str_replace("%fq_table%", $this->aliases->{$right_table}, ' '.SQLTerms::SQL_REFERENCES);
                }
                else {
                    $value = str_replace("%fq_table%",  $this->prefix."_".$right_table, ' '.SQLTerms::SQL_REFERENCES);
                }
                $this->add_field(str_replace("%key%", $right_table."_id", SQLTerms::SQL_FOREIGN_KEY), $value, $statement);
            }

            // unique constraints
            switch (@$desc->type) {
                case "1:1": $this->add_field(
                    str_replace("%left_table%", $left_table, str_replace("%right_table%", $right_table, SQLTerms::SQL_TWO_KEYS_UNIQUE)), "", $statement
                ); break;
                case "1:n": $this->add_field(
                    str_replace("%table%", $right_table, SQLTerms::SQL_SINGLE_KEY_UNIQUE), "", $statement
                ); break;
                case "n:1": $this->add_field(
                    str_replace("%table%", $left_table, SQLTerms::SQL_SINGLE_KEY_UNIQUE), "", $statement
                ); break;
            }

            // outro
            $statement .= SQLTerms::SQL_CREATE_OUTRO;

            return $statement;
        }

        /**
         * Make SQL DELETE statement
         * @param {string} table_name
         * @return
         */
        public function make_sql_delete_statement ($table_name) {
            return str_replace("%table_name%", $table_name, SQLTerms::SQL_DELETE);
        }

        /**
         * Make SQL DROP statement
         * @param {string} table_name
         * @return
         */
        public function make_sql_drop_table_statement ($table_name) {
            return str_replace("%table_name%", $table_name, SQLTerms::SQL_DROP_TABLE);
        }

        /**
         * Make SQL DROP statement
         * @param {string} table_name
         * @return
         */
        public function make_sql_drop_view_statement ($table_name) {
            return str_replace("%table_name%", $table_name, SQLTerms::SQL_DROP_VIEW);
        }

        /**
         * Make SQL list query
         * @param {string} table_name
         * @param {string} preview_fields
         * @return
         */
        public function make_sql_list_query ($table_name, $preview_fields, $order) {
            return str_replace(" %order_by%", $order, str_replace("%preview_fields%", $preview_fields, str_replace("%table_name%", $table_name, SQLTerms::SQL_DISTINCT_LIST)));
        }

        /**
         * Make SQL filter query
         * @param {string} table_name
         * @param {string} preview_fields
         * @return
         */
        public function make_sql_filter_query ($table_name, $preview_fields, $order) {
            return str_replace(" %order_by%", $order, str_replace("%preview_fields%", $preview_fields, str_replace("%table_name%", $table_name, SQLTerms::SQL_FILTER)));
        }

        /**
         * Make SQL filter query
         * @param {string} table_name
         * @param {string} preview_fields
         * @return
         */
        public function make_sql_filter_group_query ($table_name, $preview_fields, $order) {
            return str_replace(" %order_by%", $order, str_replace("%preview_fields%", $preview_fields, str_replace("%table_name%", $table_name, SQLTerms::SQL_FILTER_GROUP)));
        }

        /**
         * Make SQL CREATE VIEW statement
         * @param {string} view_name
         * @param {string} table_name
         * @param {string} preview_fields
         * @return
         */
        public function make_sql_create_view_statement ($view_name, $table_name, $preview_fields, $distinct) {
            return str_replace(
                " %order_by%", "", str_replace(
                  "%view_name%", $view_name, str_replace(
                      "%preview_fields%", $preview_fields, str_replace(
                          "%table_name%", $table_name, $distinct ? SQLTerms::SQL_CREATE_DISTINCT_VIEW : SQLTerms::SQL_CREATE_VIEW
                      )
                  )
                )
            );
        }

        /**
         * Make SQL CREATE VIEW statement
         * @param {string} view_name
         * @param {string} table_name
         * @param {string} preview_fields
         * @return
         */
        public function make_sql_create_view_group_statement ($view_name, $table_name, $preview_fields, $group_by, $distinct) {
            return str_replace(
                " %order_by%", "", str_replace(
                  "%view_name%", $view_name, str_replace(
                      "%preview_fields%", $preview_fields, str_replace(
                        "%group_by%", $group_by, str_replace(
                          "%table_name%", $table_name, $distinct ? SQLTerms::SQL_CREATE_DISTINCT_VIEW_GROUP : SQLTerms::SQL_CREATE_VIEW_GROUP
                      )
                      )
                  )
                )
            );
        }

        /**
         * Make insert method
         * @param {string} &methods
         */
        public function make_create_method ($desc, &$methods, $left_table, $right_table) {
        $valueFilter = $this->make_value_filter($desc, $left_table, $right_table);
        $methods.=<<<EOT
        /**
         * Create new dataset
         * @param object \$value
         * @return
         */
        function insert(\$value) {
            $valueFilter
            \$query_template = \$this->get_query_template("insert");
            \$result = \$this->query(\$query_template, \$value);
            \$lastid = \$this->getlastid();
            return \$lastid;
        }

EOT;
        $methods.=PHP_EOL;
        }

        /**
         * Make read method
         * @param {string} &methods
         */
        private function make_read_method (&$methods) {
        $methods.=<<<EOT
        /**
         * Read dataset
         * @param integer \$id
         * @return
         */
        function read(\$id) {
            \$query_template = \$this->get_query_template("read");
            \$result = \$this->query_one(\$query_template, \$id);
            return \$result;
        }

EOT;
        $methods.=PHP_EOL;
        }

        /**
         * Make update method
         * @param {string} &methods
         */
        private function make_update_method ($desc, &$methods, $left_table, $right_table) {
        $valueFilter = $this->make_value_filter($desc, $left_table, $right_table);
        $methods.=<<<EOT
        /**
         * Update dataset
         * @param integer \$id
         * @param object \$value
         * @return
         */
        function update(\$id, \$value) {
            $valueFilter
            if (empty(\$value)) return 0;
            \$query_template = \$this->get_query_template("update");
            \$result = \$this->query(\$query_template, \$value, \$id);
            \$affected = \$this->getaffectedrows();
            return \$affected;
        }

EOT;
        $methods.=PHP_EOL;
        }

        /**
         * Make delete method
         * @param {string} &methods
         */
        private function make_delete_method (&$methods) {
        $methods.=<<<EOT
        /**
         * Delete dataset
         * @param integer \$id
         * @return
         */
        function delete(\$id) {
            \$query_template = \$this->get_query_template("delete");
            \$result = \$this->query(\$query_template, \$id);
            \$affected = \$this->getaffectedrows();
            return \$affected;
        }

EOT;
        $methods.=PHP_EOL;
        }

        /**
         * Make filter method
         * @param {string} &methods
         */
        private function make_filter_method (&$methods) {
        $methods.=<<<EOT
        /**
         * Filter datasets in domain
         * @param object \$filter
         * @return
         */
        function filter(...\$filter) {
            \$query_template = \$this->get_query_template("filter");
            \$result = \$this->query_all(\$query_template, ...\$filter);
            return \$result;
        }

EOT;
        $methods.=PHP_EOL;
        }

        /**
         * Make filter method
         * @param {string} &methods
         */
        private function make_filter_method_cached (&$methods) {
        $methods.=<<<EOT
        /**
         * Filter datasets in domain
         * @param object \$filter
         * @return
         */
        function filter_cached(...\$filter) {
            \$query_template = \$this->get_query_template("filter");
            \$result = \$this->query_all_cached(\$query_template, ...\$filter);
            return \$result;
        }

EOT;
        $methods.=PHP_EOL;
        }

        /**
         * Make filter method
         * @param {string} &methods
         */
        private function make_filter_group_method (&$methods) {
        $methods.=<<<EOT
        /**
         * Filter datasets in domain
         * @param object \$filter
         * @return
         */
        function filtergroup(...\$filters) {
            \$query_template = \$this->get_query_template("filtergroup");
            \$result = \$this->query_all(\$query_template, ...\$filters);
            return \$result;
        }

EOT;
        $methods.=PHP_EOL;
        }

        /**
         * Make filter method
         * @param {string} &methods
         */
        private function make_filter_group_method_cached (&$methods) {
        $methods.=<<<EOT
        /**
         * Filter datasets in domain
         * @param object \$filter
         * @return
         */
        function filtergroup_cached(...\$filters) {
            \$query_template = \$this->get_query_template("filtergroup");
            \$result = \$this->query_all_cached(\$query_template, ...\$filters);
            return \$result;
        }

EOT;
        $methods.=PHP_EOL;
        }

        /**
         * Make filter first method
         * @param {string} &methods
         */
        private function make_first_method (&$methods) {
        $methods.=<<<EOT
        /**
         * Filter first dataset in domain
         * @param object \$filter
         * @return
         */
        function first(\$filter) {
            \$query_template = \$this->get_query_template("filter");
            \$result = \$this->query_one(\$query_template, \$filter);
            return \$result;
        }

EOT;
        $methods.=PHP_EOL;
        }

        /**
         * Make list method
         * @param {string} &methods
         */
        private function make_list_method (&$methods) {
        $methods.=<<<EOT
        /**
         * List all datasets in domain
         * @return
         */
        function enumerate() {
            \$query_template = \$this->get_query_template("list");
            \$result = \$this->query_all(\$query_template);
            return \$result;
        }

EOT;
        $methods.=PHP_EOL;
        }

        /**
         * Make list method
         * @param {string} &methods
         */
        private function make_list_method_cached (&$methods) {
        $methods.=<<<EOT
        /**
         * List all datasets in domain
         * @return
         */
        function enumerate_cached() {
            \$query_template = \$this->get_query_template("list");
            \$result = \$this->query_all_cached(\$query_template);
            return \$result;
        }

EOT;
        $methods.=PHP_EOL;
        }

        /**
         * Make value filter
         * @param {object} desc
         */
        private function make_value_filter ($desc, $left_table, $right_table) {
            $fields = "\$fields = [";
            $confirm_fields = "\$confirm_fields = [";
            $integer_fields = "\$integer_fields = [";
            $delim = "";
            $delim2 = "";
            $idelim = "";
            foreach($this->SQL_FIELD_MAP as $field_type => $db_type) {
              if ($field_type === "confirm_fields_0") {
                  if (isset($desc->{"confirm_fields"})) {
                      foreach ($desc->{"confirm_fields"} as $field) {
                          $confirm_fields .= $delim2.'"'.$field.'"';
                          $delim2 = ",";
                      }
                  }
              }
              else {
                  if (isset($desc->{$field_type})) {
                      foreach ($desc->{$field_type} as $field) {
                          $fields .= $delim.'"'.$field.'"';
                          if (isset($desc->{"integer_fields"})) {
                            if ($field_type === "integer_fields") {
                              $integer_fields .= $idelim.'"'.$field.'"';
                              $idelim = ",";
                            }
                          }
                          $delim = ",";
                      }
                  }
                }
            }
            if ($left_table != null) {
                $fields .= $delim.'"'.$left_table.'_id'.'"';
                $delim = ",";
            }
            if ($right_table != null) {
                $fields .= $delim.'"'.$right_table.'_id'.'"';
                $delim = ",";
            }
            $fields .= "];".PHP_EOL;
            $confirm_fields .= "];".PHP_EOL;
            $integer_fields .= "];".PHP_EOL;
            $methods=<<<EOT
            $fields
            $confirm_fields
            $integer_fields
            \$model = array();
            foreach(\$value as \$key=>\$val) {
                if (in_array(\$key, \$fields)) {
                    if (\$val === "") \$val = NULL;
                    \$model[\$key] = \$val;
                }
                if (in_array(\$key, \$confirm_fields)) {
                 \$model[\$key] = \$val;
                 if (\$val === 1) {
                    \$model[\$key."_confirmed"] = date('Y-m-d');
                 }
                }
                if (in_array(\$key, \$integer_fields)) {
                    \$model[\$key] = intval(\$val);
                }
            }
            \$value = \$model;
EOT;
            $methods.=PHP_EOL;
            return $methods;
        }

        /**
         * Make CRUD methods
         * @param {object} desc
         * @param {string} domain
         * @param {string} table_name
         * @param {string} master
         * @param {string} &methods
         */
        private function make_crud_methods ($desc, $domain, $table_name, $master, &$methods, $left_table, $right_table) {
            if (strpos(@$desc->methods, "c")!==false) {
                file_put_contents("dist".DIRECTORY_SEPARATOR."queries".DIRECTORY_SEPARATOR."$domain".DIRECTORY_SEPARATOR."insert.sql", "INSERT INTO $table_name");
                $this->make_create_method($desc, $methods, $left_table, $right_table);
            }
            if (strpos(@$desc->methods, "r")!==false) {
                file_put_contents("dist".DIRECTORY_SEPARATOR."queries".DIRECTORY_SEPARATOR."$domain".DIRECTORY_SEPARATOR."read.sql", "SELECT * FROM $table_name WHERE $master"."id=%i;");
                $this->make_read_method($methods);
            }
            if (strpos(@$desc->methods, "u")!==false) {
                file_put_contents("dist".DIRECTORY_SEPARATOR."queries".DIRECTORY_SEPARATOR."$domain".DIRECTORY_SEPARATOR."update.sql", "UPDATE $table_name SET %a WHERE $master"."id=%i;");
                $this->make_update_method($desc, $methods, $left_table, $right_table);
            }
            if (strpos(@$desc->methods, "d")!==false) {
                file_put_contents("dist".DIRECTORY_SEPARATOR."queries".DIRECTORY_SEPARATOR."$domain".DIRECTORY_SEPARATOR."delete.sql", "DELETE FROM $table_name WHERE $master"."id=%i;");
                $this->make_delete_method($methods);
            }
        }

        /**
         * Make Schema actions (create, clear and drop)
         * @param {string} domain
         * @param {string} table_name
         * @param {object} desc
         * @param {string} master
         * @param {string} left_table (optional)
         * @param {string} right_table (optional)
         */
        public function make_schema_queries ($domain, $table_name, $desc, $master, $left_table = null, $right_table = null) {
            if ($master === "") {
                file_put_contents("dist".DIRECTORY_SEPARATOR."queries".DIRECTORY_SEPARATOR."$domain".DIRECTORY_SEPARATOR."create.sql", $this->make_sql_create_statement($domain, $table_name, $desc, $left_table, $right_table));
                file_put_contents("dist".DIRECTORY_SEPARATOR."queries".DIRECTORY_SEPARATOR."$domain".DIRECTORY_SEPARATOR."drop.sql", $this->make_sql_drop_table_statement($table_name));
            }
            else {
                $preview_fields = $this->provide_preview_fields($desc);
                if (isset($desc->group)) {
                file_put_contents("dist".DIRECTORY_SEPARATOR."queries".DIRECTORY_SEPARATOR."$domain".DIRECTORY_SEPARATOR."create.sql", $this->make_sql_create_view_group_statement($table_name, $right_table, $preview_fields, $this->process_group_by($desc->group), $this->process_distinct(isset($desc->distinct) ? $desc->distinct : false)));
                }
                else {
                file_put_contents("dist".DIRECTORY_SEPARATOR."queries".DIRECTORY_SEPARATOR."$domain".DIRECTORY_SEPARATOR."create.sql", $this->make_sql_create_view_statement($table_name, $right_table, $preview_fields, $this->process_distinct(isset($desc->distinct) ? $desc->distinct : false)));
                }
                file_put_contents("dist".DIRECTORY_SEPARATOR."queries".DIRECTORY_SEPARATOR."$domain".DIRECTORY_SEPARATOR."drop.sql", $this->make_sql_drop_view_statement($table_name));
            }
            file_put_contents("dist".DIRECTORY_SEPARATOR."queries".DIRECTORY_SEPARATOR."$domain".DIRECTORY_SEPARATOR."clear.sql", $this->make_sql_delete_statement($table_name));
        }

        /**
         * Make list query
         * @param {string} table_name
         * @parma {object} desc
         */
        public function make_list_query ($table_name, $domain, $desc) {
            if (isset($desc->{'preview_fields'})) {
                    $desc->{'preview_fields'} = array_map(function($entry) {
                        $ar = explode(" AS ", $entry);
                        return array_pop($ar);
                    }, $desc->{'preview_fields'});
            }
            $preview_fields = $this->provide_preview_fields($desc);
            $order = "";
            if (isset($desc->{'order'})) {
              $order = " ORDER BY ";
              $sort_delim = "";
              foreach($desc->{'order'} as $sort_attr) {
                $order = $order . $sort_delim . $sort_attr;
                $sort_delim = ", ";
              }
            }
            file_put_contents("dist".DIRECTORY_SEPARATOR."queries".DIRECTORY_SEPARATOR."$domain".DIRECTORY_SEPARATOR."list.sql", $this->make_sql_list_query($table_name, $preview_fields, $order));
        }

        /**
         * Make filter query
         * @param {string} table_name
         * @parma {object} desc
         */
        public function make_filter_query ($table_name, $domain, $desc) {
            if (isset($desc->{'preview_fields'})) {
                    $desc->{'preview_fields'} = array_map(function($entry) {
                        $ar = explode(" AS ", $entry);
                        return array_pop($ar);
                    }, $desc->{'preview_fields'});
            }
            $preview_fields = $this->provide_preview_fields($desc);
            $order = "";
            if (isset($desc->{'order'})) {
              $order = " ORDER BY ";
              $sort_delim = "";
              foreach($desc->{'order'} as $sort_attr) {
                $order = $order . $sort_delim . $sort_attr;
                $sort_delim = ", ";
              }
            }
            file_put_contents("dist".DIRECTORY_SEPARATOR."queries".DIRECTORY_SEPARATOR."$domain".DIRECTORY_SEPARATOR."filter.sql", $this->make_sql_filter_query($table_name, $preview_fields, $order));
        }

        /**
         * Make filter group query
         * @param {string} table_name
         * @parma {object} desc
         */
        public function make_filter_group_query ($table_name, $domain, $desc) {
            if (isset($desc->{'preview_fields'})) {
                    $desc->{'preview_fields'} = array_map(function($entry) {
                        $ar = explode(" AS ", $entry);
                        return array_pop($ar);
                    }, $desc->{'preview_fields'});
            }
            $preview_fields = $this->provide_preview_fields($desc);
            $order = "";
            if (isset($desc->{'order'})) {
              $order = " ORDER BY ";
              $sort_delim = "";
              foreach($desc->{'order'} as $sort_attr) {
                $order = $order . $sort_delim . $sort_attr;
                $sort_delim = ", ";
              }
            }
            file_put_contents("dist".DIRECTORY_SEPARATOR."queries".DIRECTORY_SEPARATOR."$domain".DIRECTORY_SEPARATOR."filtergroup.sql", $this->make_sql_filter_group_query($table_name, $preview_fields, $order));
        }

        /**
         * Make DAO
         * @param {string} domain
         * @param {string} Table
         * @param {string} table_name
         * @param {object} desc
         * @param {string} master (optional)
         */
        public function make_dao ($domain, $Table, $table_name, $desc, $master = "", $left_table = null, $right_table = null) {
            $this->make_schema_queries($domain, $table_name, $desc, $master, $left_table, $right_table);
            $this->make_list_query($table_name, $domain, $desc);
            $this->make_filter_query($table_name, $domain, $desc);
            $this->make_filter_group_query($table_name, $domain, $desc);
            $db_proto = file_get_contents(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."prototypes".DIRECTORY_SEPARATOR."db.prototype.php");
            $db_proto = str_replace("%Table%", $Table, $db_proto);
            $db_proto = str_replace("%table%", $domain, $db_proto);
            $db_proto = str_replace("%vendor%", $this->vendor, $db_proto);
            $methods = "";
            $this->make_crud_methods($desc, $domain, $table_name, $master, $methods, $left_table, $right_table);
            $this->make_list_method($methods);
            $this->make_filter_method($methods);
            $this->make_filter_group_method($methods);
            $this->make_list_method_cached($methods);
            $this->make_filter_method_cached($methods);
            $this->make_filter_group_method_cached($methods);
            $this->make_first_method($methods);
            $db_proto = str_replace("%methods%", $methods, $db_proto);
            file_put_contents("dist".DIRECTORY_SEPARATOR.$this->prefix.DIRECTORY_SEPARATOR."database".DIRECTORY_SEPARATOR."db.$domain.php", $db_proto);
        }


        /**
         * Replace Database variable
         * @param {string} haystack
         * @return
         */
        private function replace_dbvar ($haystack) {
            return str_replace("§.", "`%DB::default%`" . ".", $haystack);
        }

        /**
         * Replace prefix
         * @param {string} haystack
         * @return
         */
        private function replace_prefix ($haystack) {
            return str_replace("§`", "`".$this->prefix."_", $haystack);
        }

        /**
         * Provide preview fields
         * @param {object} $desc
         * @return
         */
        private function provide_preview_fields ($desc) {
            $preview_fields = isset($desc->{'preview_fields'}) ? $this->replace_alias(implode(", ", $desc->{'preview_fields'})) : "id";
            $preview_fields = $this->replace_dbvar($this->replace_prefix($preview_fields));
            return $preview_fields;
        }

        private function process_group_by ($groups) {
           $result = "";
           $delim = "";
           foreach($groups as $group) {
            $group = $this->replace_dbvar($this->replace_prefix($group));
            $result .= $delim . "$group";
            $delim = ",";
           }
           return $result;
        }

        private function process_distinct ($distinct) {
           return $distinct ? true : false;
        }

        /**
         * Replace Aliases
         * @param {string} table_domain
         * @return
         */
        private function replace_alias ($table_domain) {
            $aliases_keys = array();
            foreach(array_keys((array) $this->aliases) as $el) {
                array_push($aliases_keys, "§"."`".$el."`");
            }
            $aliases_vals = array_values((array) $this->aliases);
            return str_replace($aliases_keys, $aliases_vals, $table_domain);
        }
        
        /**
         * Tell if join type is UNION or null if not set at all
         * @param {string} join_type
         * @return
         */
        private function is_union ($desc) {
            return isset($desc->{'join_type'}) ? ($desc->{'join_type'} === "UNION") : null;
        }
        
        /**
         * Tell if string is enclosed by characters
         * @param {string} haystack
         * @param {string} open_char
         * @param {string} close_char
         * @return
         */
        private function is_enclosed ($haystack, $open_char, $close_char) {
            return (substr($haystack, 0, 1) === $open_char && substr($haystack, -1, 1) === $close_char);
        }
        
        /**
         * Tell if string is starting with any certain character
         * @param {string} haystack
         * @param {array} char_ar
         * @return
         */
        private function is_first ($haystack, $char_ar) {
            foreach($char_ar as $first_char) {
                if (strpos($haystack, $first_char) === 0) {
                    return true;
                }
            }
            return false;
        }
        
        /**
         * Get JOINS
         * @param {object} desc
         * @return
         */
        public function get_joins ($desc) {
            $tables = "";
            $delim = "";
            $is_union = $this->is_union($desc);
            $has_multiple = count($desc->domains) > 1;
            foreach($desc->domains as $table_domain) {
                if ($this->is_enclosed($table_domain, "[", "]")) {
                    $table_domain = substr($table_domain, 1, -1);
                    $delim = " LEFT JOIN ";
                }
                $aliased = $this->replace_alias((($this->is_first($table_domain, ["§", "("]))?"":"§").$table_domain);
                $expanded = $this->replace_dbvar($this->replace_prefix($aliased));
                if ($is_union && $has_multiple && $this->is_enclosed($expanded, "(", ")")) {
                    $expanded = "%SEL%" . $expanded;
                }
                $tables .= $delim . $expanded;
                if ($is_union === true) {
                    $delim = " " . $desc->{'join_type'} . " ";
                }
                else if ($is_union === false) {
                    $delim = " " . $desc->{'join_type'} . " JOIN ";
                }
                else {
                    $delim = " CROSS JOIN ";
                }
            }
            if (isset($desc->{'join_alias'})) {
                $tables = "(" . $tables . ")" . " AS " . $desc->{'join_alias'};
            }
            return $tables;
        }
    }
?>
