#!/usr/bin/env php
<?php
	namespace LSDL\protogen;

	require_once('vendor/autoload.php');

	use LSDL\protogen\lib\Generator;

	$generator = new Generator();
	$generator->generate();
?>
