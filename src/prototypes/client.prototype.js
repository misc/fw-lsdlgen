/**
 * Wrapper for getting a DOM element by ID
 * Returns null if no matching element was found
 * @param {string} el_id
 * @returns {DOMElement|null}
 */
function %prefix%_get_element_by_id(el_id) {
    "use strict";
    return document.getElementById(el_id);
}

/**
 * Wrapper for getting a DOM element by name
 * Returns null if no matching element was found
 * @param {string} el_id
 * @returns {DOMElement|null}
 */
function %prefix%_get_element_by_name(el_name) {
    "use strict";
    return (document.getElementsByName(el_name)[0] || null);
}

/**
 * Transform HTMLCollection or NodeList into array
 * @param {HTMLCollection|NodeList}
 * @returns {Array<DOMElement>}
 */
function %prefix%_make_collection_to_array(collection) {
    "use strict";
    var result = [],
        i = 0;
    for (i = 0; i < collection.length; i++) {
        result.push(collection[i]);
    }
    // cleanup references
    collection = null;
    return result;
}

/**
 * Wrapper for getting DOM elements by tag name
 * @param {string} tag_name
 * @param {DOMElement} sub_element
 * @returns {Array}
 */
function %prefix%_get_elements_by_tag(tag_name, sub_element) {
    "use strict";
    var result = null;
    if (sub_element == null) {
        result = document.getElementsByTagName(tag_name);
    } else if (typeof sub_element.getElementsByTagName != "function") {
        result = sub_element.querySelectorAll(tag_name);
    } else {
        result = sub_element.getElementsByTagName(tag_name);
    }
    // cleanup references
    sub_element = null;
    return %prefix%_make_collection_to_array(result);
}

/**
 * Wrapper for getting DOM elements by class name
 * @param {string} class_name
 * @param {DOMElement} sub_element
 * @returns {Array}
 */
function %prefix%_get_elements_by_class(class_name, sub_element) {
    "use strict";
    var result = null;
    if (sub_element == null) {
        result = document.getElementsByClassName(class_name);
    } else if (typeof sub_element.getElementsByClassName != "function") {
        result = sub_element.querySelectorAll("." + class_name);
    } else {
        result = sub_element.getElementsByClassName(class_name);
    }
    // cleanup references
    sub_element = null;
    return %prefix%_make_collection_to_array(result);
}

/**
 * Remove all child nodes inside a DOM element
 * @param {DOMElement} el
 */
function %prefix%_clear_element(el) {
    "use strict";
    while (el.hasChildNodes()) {
        el.removeChild(el.lastChild);
    }
    // cleanup references
    el = null;
}

/**
 * Import node from template into document
 * @param {HTMLTemplateElement}
 * @returns {DOMElement}
 */
function %prefix%_import_node(t) {
    "use strict";
    // this could break universal compatibility
    return document.importNode(t.content, true);
}

/**
 * Check if an element should be hidden
 * @param {DOMElemen}
 * @returns {boolean}
 */
function %prefix%_is_exposed(el) {
    "use strict";
    if (el.hasAttribute("type") && el.getAttribute("type") === "hidden") {
        return true;
    }
    return !el.classList.contains("invisible");
}

/**
 * Fill flat model from form input element
 * Reads and transforms values
 * @param {Object} model_map
 * @param {DOMElement} input_el
 */
function %prefix%_fill_model_from_form_input(model_map, input_el) {
    "use strict";
    var el = null;
    if (input_el.name.length > 0 && %prefix%_is_exposed(input_el)) {
        if (input_el.type === "checkbox") {
            model_map[input_el.name] = input_el.checked ? 1 : 0;
        } else {
            if (input_el.type !== "file") {
                if (input_el.type == "number") {
                    model_map[input_el.name] = Number(input_el.value);
                } else {
                    model_map[input_el.name] = input_el.value;
                }
            } else {
                model_map[input_el.name] = null;
                el = %prefix%_get_elements_by_class("filechooser-image", input_el.parentNode);
                if (el != null) {
                    el = el[0];
                    if (el != null) {
                        model_map[input_el.name] = el.getAttribute("data-identifier");
                        el = null;
                    }
                }
            }
        }
    }
    // cleanup references
    model_map = null;
    input_el = null;
}

/**
 * Fill flat model from form select element
 * Reads and transforms values
 * @param {Object} model_map
 * @param {DOMElement} select_el
 */
function %prefix%_fill_model_from_form_select(model_map, select_el) {
    "use strict";
    if (select_el.name.length > 0 && %prefix%_is_exposed(select_el)) {
        model_map[select_el.name] = select_el.value;
    }
    // cleanup references
    model_map = null;
    select_el = null;
}

/**
 * Get flat model from form
 * @param {DOMElement} form_el
 * @returns {Object}
 */
function %prefix%_get_model_from_form(form_el) {
    "use strict";
    var model_map = {},
        inputs = %prefix%_get_elements_by_tag("input", form_el),
        selects = %prefix%_get_elements_by_tag("select", form_el),
        i = null;
    for (i = 0; i < inputs.length; i++) {
        %prefix%_fill_model_from_form_input(model_map, inputs[i]);
    }
    for (i = 0; i < selects.length; i++) {
        %prefix%_fill_model_from_form_select(model_map, selects[i]);
    }
    // cleanup references
    form_el = null;
    inputs = null;
    selects = null;
    return model_map;
}

/**
 * Trace debug notifications
 * @param {any} arg0
 * @param {any} arg1
 * @param {any} arg2
 */
function %prefix%_trace(arg0, arg1, arg2) {
    "use strict";
    if (window.hasOwnProperty("%prefix%_debug_mode")) {
        if (window.%prefix%_debug_mode === true) {
            console.trace(arg0, arg1, arg2);
        }
    }
    arg0 = null;
    arg1 = null;
    arg2 = null;
}

/**
 * Get store by name
 * @param {string} store_name
 * @returns {Object}
 */
function %prefix%_get_store(store_name) {
    "use strict";
    if (window.hasOwnProperty(store_name)) {
        return window[store_name];
    }
    window[store_name] = {};
    return window[store_name];
}

/**
 * Get store attribute by name
 * @param {string} store_name
 * @param {string} attr
 * @returns {Object}
 */
function %prefix%_get_store_attr(store_name, attr) {
    "use strict";
    var store = %prefix%_get_store(store_name);
    if (!store.hasOwnProperty(attr)) {
        store[attr] = {};
    }
    return store[attr];
}

/**
 * Register listener for element and type
 * Any listener that already exists for element and type will be replaced
 * @param {DOMElement} el
 * @param {string} type
 * @param {function} listener
 * @param {boolean} allow_multi
 */
function %prefix%_update_listener(el, type, listener, allow_multi) {
    "use strict";
    var store = null,
        id = null;
    if (!el || !el.getAttribute) {
        %prefix%_trace("update_listener", "missing", el);
        // cleanup references
        el = null;
        listener = null;
        return;
    }
    id = el.getAttribute("id");
    store = %prefix%_get_store_attr("%prefix%_global_listeners", type);
    if (!allow_multi) {
        if (store.hasOwnProperty(id)) {
            el.removeEventListener(type, store[id]);
        }
        el.addEventListener(type, listener);
        store[id] = listener;
    } else {
        if (!store.hasOwnProperty(id)) {
            store[id] = [];
        }
        el.addEventListener(type, listener);
        store[id].push(listener);
    }
    // cleanup references
    el = null;
    listener = null;
    store = null;
}

/**
 * Register listener for element and type
 * Any listener that already exists for element and type will be replaced
 * @param {DOMElement} el
 * @param {string} type
 * @param {function} listener
 * @param {boolean} allow_multi
 */
function %prefix%_clear_multi_listener(el, type) {
    "use strict";
    var store = null,
        id = null;
    store = %prefix%_get_store_attr("%prefix%_global_listeners", type);
    id = el.getAttribute("id");
    if (store.hasOwnProperty(id)) {
        store[id] = [];
    }
    // cleanup references
    el = null;
    store = null;
}

/**
 * Add API interceptor if not already registered for service name
 * @param {string} name
 * @param {function} interceptor
 */
function %prefix%_register_api_interceptor(name, interceptor) {
    "use strict";
    var store = %prefix%_get_store_attr(
        "%prefix%_global_interceptors",
        name
    );
    if (!store.hasOwnProperty("interceptor")) {
        store.interceptors = [];
    }
    if (store.interceptors.indexOf(interceptor) === -1) {
        store.interceptors.push(interceptor);
    }
    // cleanup references
    interceptor = null;
    store = null;
}

/**
 * Add hook if not already registered for service name
 * @param {string} name
 * @param {function} interceptor
 */
function %prefix%_register_gui_hook(name, hook) {
    "use strict";
    var store = %prefix%_get_store_attr(
        "%prefix%_global_hooks",
        name
    );
    if (!store.hasOwnProperty("hooks")) {
        store.hooks = [];
    }
    if (store.hooks.indexOf(hook) === -1) {
        store.hooks.push(hook);
    }
    // cleanup references
    hook = null;
    store = null;
}

/**
 * Get API interceptor data for service name
 * @param {string} name
 * @returns {Object}
 */
function %prefix%_get_api_interceptors(name) {
    "use strict";
    return %prefix%_get_store_attr(
        "%prefix%_global_interceptors",
        name
    );
}

/**
 * Call all registered API interceptors as chain and return transformed response
 * @param {string} action
 * @param {Object} payload
 * @param {Object} response
 */
function %prefix%_call_api_interceptors(action, payload, response) {
    "use strict";
    var interceptors = %prefix%_get_api_interceptors(action).interceptors,
        i = null;
    %prefix%_trace(action, %prefix%_get_api_interceptors(action), interceptors);
    if (interceptors != null) {
        for (i = 0; i < interceptors.length; i++) {
            response = interceptors[i](payload, response);
        }
    }
    // cleanup references
    interceptors = null;
    payload = null;
    return response;
}

/**
 * Get GUI hook data for name
 * @param {string} name
 * @returns {Object}
 */
function %prefix%_get_gui_hooks(name) {
    "use strict";
    return %prefix%_get_store_attr(
        "%prefix%_global_hooks",
        name
    );
}

/**
 * Call all registered GUI hooks as chain and return transformed response
 * @param {string} name
 */
function %prefix%_call_gui_hooks(name, target, payload) {
    "use strict";
    var hooks = %prefix%_get_gui_hooks(name).hooks,
        i = null,
        response = null;
    %prefix%_trace(name, %prefix%_get_gui_hooks(name), hooks);
    if (hooks != null) {
        for (i = 0; i < hooks.length; i++) {
            response = hooks[i](target, payload, response);
        }
    }
    // cleanup references
    hooks = null;
    target = null;
    payload = null;
    return response;
}

/**
 * Schedule XHR callback for deferred execution
 * @param {number} status
 * @param {string} action
 * @param {Object} payload
 * @param {Object} response
 * @param {function} cb
 */
function %prefix%_schedule_xhr_callback(status, action, payload, response, cb) {
    "use strict";
    setTimeout(function () {
        cb(
            status,
            %prefix%_call_api_interceptors(action, payload, response)
        );
        // cleanup references
        payload = null;
        response = null;
        cb = null;
    }, 0);
}

/**
 * Make onload handler for XHR
 * @param {string} action
 * @param {Object} payload
 * @param {function} callback
 * @returns {function}
 */
function %prefix%_make_xhr_load_handler(action, payload, callback) {
    "use strict";
    var func = null;
    func = function (e) {
        var response = null,
            type = null;
        // if request was successful
        if (e.target.status === 200) {
            type = e.target.responseType;
            if (type === "json") {
                response = e.target.response;
            } else {
                // this is a fallback in case content-type header is missing
                try {
                    response = JSON.parse(e.target.responseText);
                } catch (error) {
                    %prefix%_trace("api_call", "xhr", error);
                    // cleanup references
                    response = null;
                }
            }
            %prefix%_schedule_xhr_callback(
                e.target.status,
                action,
                payload,
                response,
                callback
            );
            // cleanup references
            e.target.onreadystatechange = null;
            e.target.onerror = null;
            e.target.onload = null;
            payload = null;
            callback = null;
        }
        // if requested resource was not found or has thrown an error
        if (e.target.status >= 400 && e.target.status < 600) {
            %prefix%_schedule_xhr_callback(
                e.target.status,
                action,
                payload,
                null,
                callback
            );
            // cleanup references
            e.target.onreadystatechange = null;
            e.target.onerror = null;
            e.target.onload = null;
            payload = null;
            callback = null;
        }
        // cleanup references
        response = null;
        e = null;
    };
    return func;
}

/**
 * Make onerror handler for XHR
 * @param {Object} payload
 * @param {function} callback
 * @returns {function}
 */
function %prefix%_make_xhr_error_handler(payload, callback) {
    "use strict";
    var func = null;
    func = function (e) {
        %prefix%_trace("api_call", "xhr_error", payload);
        %prefix%_schedule_xhr_callback(
            null,
            null,
            payload,
            null,
            callback
        );
        // cleanup references
        e.target.onreadystatechange = null;
        e.target.onerror = null;
        e.target.onload = null;
        payload = null;
        callback = null;
        e = null;
    };
    return func;
}

/**
 * Execute XHR request
 * @param {string} endpoint
 * @param {string} action
 * @param {Object} payload
 * @param {function} callback
 */
function %prefix%_api_call_post(endpoint, action, payload, callback) {
    "use strict";
    var xhr = new XMLHttpRequest();
    xhr.open("POST", endpoint, true);
    xhr.responseType = "json";
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = %prefix%_make_xhr_load_handler(action, payload, callback);
    xhr.onerror = %prefix%_make_xhr_error_handler(payload, callback);
    xhr.send(JSON.stringify({"action": action, "payload": payload}));
    // cleanup references
    payload = null;
    callback = null;
}

/**
 * Append endpoint parameter
 * @param {string} endpoint
 * @param {string} param
 */
function %prefix%_append_endpoint_param(endpoint, param) {
    "use strict";
    if (endpoint.indexOf("?") > -1) {
        endpoint = endpoint + "&HTTP_PAYLOAD_WRAPPED=" + encodeURIComponent(param);
    } else {
        endpoint = endpoint + "?HTTP_PAYLOAD_WRAPPED=" + encodeURIComponent(param);
    }
    // cleanup references
    param = null;
    return endpoint;
}

/**
 * Execute XHR request
 * @param {string} endpoint
 * @param {string} action
 * @param {Object} payload
 * @param {function} callback
 */
function %prefix%_api_call_get(endpoint, action, payload, callback) {
    "use strict";
    var xhr = new XMLHttpRequest(),
        param = JSON.stringify({"action": action, "payload": payload});
    xhr.open("GET", %prefix%_append_endpoint_param(endpoint, param), true);
    xhr.responseType = "json";
    xhr.onload = %prefix%_make_xhr_load_handler(action, payload, callback);
    xhr.onerror = %prefix%_make_xhr_error_handler(payload, callback);
    xhr.send();
    // cleanup references
    payload = null;
    callback = null;
}

/**
 * Execute XHR request
 * @param {string} endpoint
 * @param {string} action
 * @param {Object} payload
 * @param {function} callback
 */
function %prefix%_api_call(endpoint, action, payload, callback) {
    "use strict";
    if (action.indexOf("get_") === 0) {
        %prefix%_api_call_get(endpoint, action, payload, callback);
    } else {
        %prefix%_api_call_post(endpoint, action, payload, callback);
    }
    // cleanup references
    payload = null;
    callback = null;
}

/**
 * Get endpoint address
 * @returns {string}
 */
function %prefix%_get_endpoint() {
    "use strict";
    return "%apiurl%";
}

/**
 * Download file handler
 * @param {number} status
 * @param {Object} response
 */
function %prefix%_download_file_handler(status, response) {
    "use strict";
    %prefix%_trace("download", status, response);
    if (response != null && response.link != null) {
        window.open(response.link, response.name);
    }
    // cleanup references
    response = null;
}

/**
 * Spawn download asynchronously
 * @param {string} file_name
 * @param {string} file_mime
 * @param {string} file_label
 */
function %prefix%_download_file(file_name, file_mime, file_label) {
    "use strict";
    %prefix%_api_call(
        "%apiurl%",
        "download",
        {
            file_label: file_label,
            file_mime: file_mime,
            file_name: file_name
        },
        %prefix%_download_file_handler
    );
}

/**
 * Handle trigger
 * @param {Event} e
 * @param {string} service
 * @param {Object} params
 * @param {function} special_function
 */
function %prefix%_handle_trigger(e, service, params, special_function) {
    "use strict";
    %prefix%_api_call(
        "%apiurl%",
        service,
        params,
        function (status, response) {
            if (typeof special_function == "function") {
                special_function(e);
            }
            %prefix%_trace("trigger response", status, response);
            e.target.classList.remove("active");
            // cleanup references
            e = null;
            params = null;
            special_function = null;
            response = null;
        }
    );
}

/**
 * Make trigger handler
 * @param {string} service
 * @param {Object} params
 * @param {function} special_function
 */
function %prefix%_make_trigger_handler(service, params, special_function) {
    "use strict";
    var func = null;
    func = function (e) {
        e.stopPropagation();
        if (service != null && service.length > 0 && !e.target.classList.contains("disabled")) {
            e.target.classList.add("active");
            if (window.hasOwnProperty("%prefix%_" + service)) {
                window["%prefix%_" + service](params, function (status, response) {
                    if (typeof special_function == "function") {
                        special_function(e);
                    }
                    %prefix%_trace("trigger response", status, response);
                    e.target.classList.remove("active");
                });
            } else {
                %prefix%_handle_trigger(e, service, params, special_function);
            }
            // cleanup references
            e = null;
        }
    };
    return func;
}

/**
 * Register trigger
 * @param {DOMElement} trigger_el
 * @param {Object} model_map
 * @param {function} special_function
 */
function %prefix%_register_trigger(trigger_el, model_map, special_function) {
    "use strict";
    var listener = %prefix%_make_trigger_handler(
        trigger_el.getAttribute("data-service"),
        model_map,
        special_function
    );
    %prefix%_update_listener(trigger_el, "click", listener);
    // cleanup references
    trigger_el = null;
    model_map = null;
    special_function = null;
    listener = null;
}

/**
 * Filter list nodes
 * @param {DOMElement} list_el
 * @returns {Array}
 */
function %prefix%_filter_list_nodes(list_el) {
    "use strict";
    var items = [],
        i = null;
    for (i = 0; i < list_el.childNodes.length; i++) {
        if (list_el.childNodes[i].nodeType != 3) {
            items.push(list_el.childNodes[i]);
        }
    }
    return items;
}

/**
 * Check if multiselect item is selected
 * @param {DOMElement} item
 * @param {boolean} is_map_matching
 * @param {Array} model_maps
 * @returns {boolean}
 */
function %prefix%_check_multiselect_item(item, is_map_matching) {
    "use strict";
    var el = null;
    if (is_map_matching) {
        el = %prefix%_get_element_by_name(
            "multiselect-" + item.parentNode.getAttribute("data-name"),
            item
        );
        if (!el.checked) {
            return true;
        }
    }
    return false;
}

/**
 * Register list multiselect trigger
 * @param {DOMElement} list_el
 * @param {DOMElement} trigger_el
 * @param {Array} model_maps
 */
function %prefix%_register_list_multiselect_trigger(list_el, trigger_el, model_maps) {
    "use strict";
    var listener = null;
    listener = function (e) {
        var i = null,
            items = null,
            will_cont = null,
            params = null,
            service = e.target.getAttribute("data-service");
        items = %prefix%_filter_list_nodes(list_el);
        for (i = 0; i < model_maps.length; i++) {
            will_cont = %prefix%_check_multiselect_item(
                items[i],
                (items.length === model_maps.length)
            );
            if (!will_cont) {
                params = model_maps[i];
                %prefix%_make_trigger_handler(service, params, null)({
                    "target": e.target
                });
            }
        }
        // cleanup references
        items = null;
        e = null;
    };
    %prefix%_update_listener(trigger_el, "click", listener);
    // cleanup references
    listener = null;
}

/**
 * Register list multiselect triggers
 * @param {DOMElement} list_el
 * @param {Array} model_maps
 */
function %prefix%_register_list_multiselect_triggers(list_el, model_maps) {
    "use strict";
    var triggers = null,
        i = null;
    triggers = %prefix%_get_elements_by_class("autolist-trigger");
    for (i = 0; i < triggers.length; i++) {
        if (
            triggers[i].hasAttribute("data-for-id") && (triggers[i].getAttribute("data-for-id") === list_el.id)
        ) {
            %prefix%_register_list_multiselect_trigger(
                list_el,
                triggers[i],
                model_maps
            );
        }
    }
    // cleanup references
    list_el = null;
    triggers = null;
    model_maps = null;
}

/**
 * Reload list by name
 * @param {string} name
 */
function %prefix%_reload_list(name) {
    "use strict";
    var store = %prefix%_get_store_attr("%prefix%_global_lists", name);
    if (store != null) {
        store.load();
    }
    // cleanup references
    store = null;
}

/**
 * Set list load parameters by name
 * @param {string} name
 * @param {Object} model
 */
function %prefix%_set_list_params(name, model) {
    "use strict";
    var store = %prefix%_get_store_attr("%prefix%_global_lists", name);
    if (store != null) {
        store.load_params = model;
    }
    // cleanup references
    store = null;
    model = null;
}

/**
 * Get list load parameters by name
 * @param {string} name
 * @returns {Object}
 */
function %prefix%_get_list_params(name) {
    "use strict";
    var store = %prefix%_get_store_attr("%prefix%_global_lists", name),
        result = null;
    if (store != null) {
        result = store.load_params;
        // cleanup references
        store = null;
        return result;
    }
    // cleanup references
    store = null;
    return null;
}

/**
 * Write toggle value to list item
 * @param {DOMElement} el
 * @param {string} trigger_toggle_attr
 * @param {string} list_item_id
 */
function %prefix%_write_list_toggle_value(el, trigger_toggle_attr, list_item_id) {
    "use strict";
    if (el.hasAttribute("data-name")) {
        if (el.getAttribute("data-name") == trigger_toggle_attr) {
            if (Number(el.getAttribute("data-numeric-value")) === 0) {
                el.setAttribute("data-numeric-value", "1");
                %prefix%_get_element_by_id(list_item_id).setAttribute("data-toggle-value", "1");
                el.textContent = "1";
            } else if (Number(el.getAttribute("data-numeric-value")) === 1) {
                el.setAttribute("data-numeric-value", "0");
                el.textContent = "0";
                %prefix%_get_element_by_id(list_item_id).setAttribute(
                    "data-toggle-value",
                    "0"
                );
            }
            // cleanup references
            el = null;
            return true;
        }
    }
    // cleanup references
    el = null;
    return false;
}

/**
 * Make list item toggle handler
 * @param {string} list_item_id
 * @param {string} list_name
 */
function %prefix%_make_list_item_toggle_handler(list_item_id, list_name) {
    "use strict";
    var func = function (e) {
        var els = null,
            i = null,
            toggle_attr = e.target.getAttribute("data-toggle");
        if (toggle_attr != "") {
            els = %prefix%_get_elements_by_tag("span", %prefix%_get_element_by_id(list_item_id));
            for (i = 0; i < els.length; i++) {
                if (%prefix%_write_list_toggle_value(els[i], toggle_attr, list_item_id)) {
                    break;
                }
            }
        } else {
            %prefix%_reload_list(list_name);
        }
        // cleanup references
        els = null;
        e = null;
    };
    return func;
}

/**
 * Register list trigger
 * @param {DOMElement} trigger_el
 * @param {Object} model_map
 * @param {string} list_name
 * @param {string} list_item_id
 */
function %prefix%_register_list_trigger(trigger_el, model_map, list_name, list_item_id) {
    "use strict";
    %prefix%_register_trigger(
        trigger_el,
        model_map,
        %prefix%_make_list_item_toggle_handler(list_item_id, list_name)
    );
    // cleanup references
    trigger_el = null;
    model_map = null;
}

/**
 * Register form trigger
 * @param {DOMElement} trigger_el
 * @param {Object} model_map
 */
function %prefix%_register_form_trigger(trigger_el, model_map) {
    "use strict";
    %prefix%_register_trigger(trigger_el, model_map, null);
    // cleanup references
    trigger_el = null;
    model_map = null;
}

/**
 * Get form links by name
 * @param {string} name
 * @returns {Array}
 */
function %prefix%_get_formlinks(name) {
    "use strict";
    var store = %prefix%_get_store_attr("%prefix%_global_forms", name);
    if (store != null) {
        if (store.links == null) {
            store.links = [];
        }
        return store.links;
    }
    %prefix%_trace("no formlinks found for", name, null);
    return [];
}

/**
 * Get original form content by name
 * @param {string} name
 * @returns {Object}
 */
function %prefix%_get_formoriginal(name) {
    "use strict";
    var store = %prefix%_get_store_attr("%prefix%_global_forms", name);
    if (store != null) {
        if (store.original == null) {
            store.original = {};
        }
        return store.original;
    }
    %prefix%_trace("no formoriginal found for", name, null);
    return {};
}

/**
 * Make editor delete handler
 * @param {function} handler
 * @returns {function}
 */
function %prefix%_make_editor_delete_handler(handler) {
    "use strict";
    var func = null;
    func = function (e) {
        if (window.hasOwnProperty("%prefix%_editor_delete_callback")) {
            window.%prefix%_editor_delete_callback(function () {
                handler(e);
            }, function () {
                %prefix%_trace("editor_delete", "decision", "no");
            });
        } else {
            handler(e);
        }
    };
    return func;
}

/**
 * Register editor delete handler
 * @param {DOMElement} form_el
 * @param {DOMElement} button_el
 * @param {Object} model_map
 * @param {function} reset_form
 */
function %prefix%_register_editor_delete(service, button_el, model_map, reset_form) {
    "use strict";
    var f = null,
        g = null;
    f = %prefix%_make_trigger_handler(service, {model: model_map}, reset_form);
    g = %prefix%_make_editor_delete_handler(f);
    %prefix%_update_listener(button_el, "click", g);
    // cleanup references
    f = null;
    g = null;
    button_el = null;
    model_map = null;
    reset_form = null;
}

/**
 * Make reload formlinks handler
 * @param {DOMElement} form_el
 * @param {Object} original
 * @param {function} write_model_to_form_func
 * @returns {function}
 */
function %prefix%_make_reload_formlinks_handler(form_el, original, write_model_to_form_func) {
    "use strict";
    var func = function () {
        var formlinks = %prefix%_get_formlinks(form_el.getAttribute("name")),
            i = null;
        write_model_to_form_func(form_el, original);
        for (i = 0; i < formlinks.length; i++) {
            formlinks[i].clear();
            formlinks[i].load();
        }
        // cleanup references
        formlinks = null;
    };
    return func;
}

/**
 * Make reset formlinks handler
 * @param {DOMElement} form_el
 * @param {Object} original
 * @param {function} write_model_to_form_func
 * @returns {function}
 */
function %prefix%_make_reset_formlinks_handler(form_el, original, write_model_to_form_func) {
    "use strict";
    var func = function () {
        var formlinks = %prefix%_get_formlinks(form_el.getAttribute("name")),
            i = null;
        write_model_to_form_func(form_el, original);
        for (i = 0; i < formlinks.length; i++) {
            formlinks[i].reset();
        }
        form_el.classList.remove("inactive");
        form_el.classList.add("active");
        // cleanup references
        formlinks = null;
    };
    return func;
}

/**
 * Make reset form handler
 * @param {DOMElement} form_el
 * @param {function} write_model_to_form_func
 * @returns {function}
 */
function %prefix%_make_reset_form_handler(form_el, write_model_to_form_func) {
    "use strict";
    var func = function () {
        write_model_to_form_func(form_el, {
            id: null
        });
        form_el.classList.remove("inactive");
        form_el.classList.add("active");
    };
    return func;
}

/**
 * Make download file handler
 * @param {string} file_name
 * @param {string} file_mime
 * @param {string} file_label
 * @returns {function}
 */
function %prefix%_make_download_file_handler(file_name, file_mime, file_label) {
    "use strict";
    return function () {
        %prefix%_download_file(file_name, file_mime, file_label);
    };
}

/**
 * Handler for changes on filechooser
 * @param {DOMEvent} e
 */
function %prefix%_filechooser_handler(e) {
    "use strict";
    var h_addendum_el = null,
        h_img_el = null;
    if (e.target.files.length === 0) {
        h_addendum_el = %prefix%_get_element_by_id(e.target.getAttribute("id") + "-addendum");
        h_addendum_el.textContent = "";
        e.target.removeAttribute("data-file-mimetype");
        e.target.removeAttribute("data-file-ticketid");
        e.target.textContent = "";
        e.target.value = "";
        h_img_el = %prefix%_get_elements_by_class("filechooser-image", h_addendum_el.parentNode);
        if (h_img_el.length > 0) {
            h_addendum_el.parentNode.removeChild(h_img_el[0]);
        }
    }
    // cleanup references
    h_addendum_el = null;
    h_img_el = null;
    e = null;
}

/**
 * Set readvalue attribute on select elements
 * @param {DOMElement} form_el
 * @param {Object} model_map
 */
function %prefix%_set_select_readvalues(form_el, model_map) {
    "use strict";
    var selects = %prefix%_get_elements_by_tag("select", form_el),
        i = null,
        input_el = null;
    for (i = 0; i < selects.length; i++) {
        input_el = selects[i];
        if (input_el.name.length > 0) {
            input_el.value = model_map[input_el.name];
            input_el.setAttribute("data-readvalue", model_map[input_el.name]);
        }
    }
    // cleanup refernces
    selects = null;
    input_el = null;
    form_el = null;
    model_map = null;
}

/**
 * Write value to filechooser field
 * @param {DOMElement} input_el
 * @param {Object} model_map
 */
function %prefix%_handle_filechooser_formfield(input_el, model_map) {
    "use strict";
    var identifier = null,
        parts = null,
        ticketid = null,
        mimetype = null,
        filename = null,
        addendum_el = null,
        imgel = null,
        newel = null,
        d = %prefix%_make_download_file_handler;
    input_el.classList.add("autoform-filechooser");
    identifier = model_map[input_el.name];
    if (typeof identifier == "string" && identifier != "") {
        parts = identifier.split(";", 3);
        if (parts.length === 3) {
            ticketid = parts[0];
            mimetype = parts[1];
            filename = parts[2];
            input_el.value = "";
            input_el.setAttribute("data-file-mimetype", mimetype);
            input_el.setAttribute("data-file-ticketid", ticketid);
            addendum_el = %prefix%_get_element_by_id(input_el.getAttribute("id") + "-addendum");
            addendum_el.textContent = filename;
            imgel = %prefix%_get_elements_by_class("filechooser-image", input_el.parentNode);
            if (imgel.length === 0) {
                imgel = %prefix%_get_elements_by_class("filechooser-image", input_el.parentNode.parentNode);
            }
            if (imgel.length > 0) {
                imgel[0].parentNode.removeChild(imgel[0]);
            }
            if (mimetype.indexOf("image/") === 0) {
                newel = document.createElement("span");
                newel.classList.add("filechooser-image");
                newel.setAttribute("data-identifier", identifier);
                input_el.parentNode.insertBefore(newel, input_el);
            }
            %prefix%_update_listener(addendum_el, "click", d(ticketid, mimetype, filename));
        } else {
            input_el.value = "";
            input_el.removeAttribute("data-file-mimetype");
            input_el.removeAttribute("data-file-ticketid");
            addendum_el = %prefix%_get_element_by_id(input_el.getAttribute("id") + "-addendum");
            addendum_el.textContent = identifier;
            input_el.textContent = "";
            input_el.value = "";
            imgel = %prefix%_get_elements_by_class("filechooser-image", input_el.parentNode);
            if (imgel.length === 0) {
                imgel = %prefix%_get_elements_by_class("filechooser-image", input_el.parentNode.parentNode);
            }
            if (imgel.length > 0) {
                imgel[0].parentNode.removeChild(imgel[0]);
            }
            if (identifier.indexOf("files/") === 0) {
                newel = document.createElement("span");
                newel.classList.add("filechooser-image");
                newel.setAttribute("data-identifier", identifier);
                input_el.parentNode.insertBefore(newel, input_el);
            }
        }
    } else {
        input_el.removeAttribute("data-file-mimetype");
        input_el.removeAttribute("data-file-ticketid");
        addendum_el = %prefix%_get_element_by_id(input_el.getAttribute("id") + "-addendum");
        addendum_el.textContent = "";
        input_el.textContent = "";
        input_el.value = "";
        imgel = %prefix%_get_elements_by_class("filechooser-image", input_el.parentNode);
        if (imgel.length === 0) {
            imgel = %prefix%_get_elements_by_class("filechooser-image", input_el.parentNode.parentNode);
        }
        if (imgel.length > 0) {
            imgel[0].parentNode.removeChild(imgel[0]);
        }
    }
}

/**
 * Write model to form
 * @param {DOMElement} form_el
 * @param {Object} model_map
 */
function %prefix%_write_model_to_form(form_el, model_map) {
    "use strict";
    var inputs = %prefix%_get_elements_by_tag("input", form_el),
        i = null,
        input_el = null,
        triggers = null,
        filechooser = null,
        date = null,
        f = null,
        g = null,
        h = null,
        original = null;
    original = %prefix%_get_formoriginal(form_el.getAttribute("name"));
    f = %prefix%_make_reload_formlinks_handler(form_el, original, %prefix%_write_model_to_form);
    g = %prefix%_make_reset_formlinks_handler(form_el, original, %prefix%_write_model_to_form);
    h = %prefix%_make_reset_form_handler(form_el, %prefix%_write_model_to_form);
    %prefix%_set_select_readvalues(form_el, model_map);
    for (i = 0; i < inputs.length; i++) {
        input_el = inputs[i];
        if (input_el.name.length > 0) {
            switch (input_el.getAttribute("type")) {
            case "file":
                %prefix%_handle_filechooser_formfield(input_el, model_map);
                break;
            case "date":
                if (typeof model_map[input_el.name] == "string") {
                    input_el.value = model_map[input_el.name];
                }
                if (typeof model_map[input_el.name] == "object") {
                    if (model_map[input_el.name] != null) {
                        date = model_map[input_el.name].date.split(" ").shift();
                        input_el.value = date;
                    } else {
                        input_el.value = ""; // for null values
                    }
                }
                break;
            case "checkbox":
                if (model_map[input_el.name] === 1) {
                    input_el.checked = true;
                } else {
                    input_el.checked = false;
                }
                break;
            default:
                if (model_map.hasOwnProperty(input_el.name)) {
                    input_el.value = model_map[input_el.name];
                }
                break;
            }
            if (input_el.hasAttribute("data-fromeditor")) {
                input_el.setAttribute("data-fromeditor", null);
            }
        } else {
            switch (input_el.getAttribute("type")) {
            case "button":
                if (input_el.getAttribute("data-name") === "create") {
                    %prefix%_update_listener(input_el, "click", g);
                }
                if (input_el.getAttribute("data-name") === "delete") {
                    %prefix%_register_editor_delete(form_el.getAttribute("data-service-delete"), input_el, model_map, f);
                }
                if (input_el.getAttribute("data-name") === "duplicate") {
                    %prefix%_update_listener(input_el, "click", h);
                }
                break;
            }
        }
    }
    triggers = %prefix%_get_elements_by_class("autoform-trigger", form_el);
    for (i = 0; i < triggers.length; i++) {
        %prefix%_register_form_trigger(triggers[i], model_map);
    }
    filechooser = %prefix%_get_elements_by_class("autoform-filechooser", form_el);
    for (i = 0; i < filechooser.length; i++) {
        %prefix%_update_listener(filechooser[i], "change", %prefix%_filechooser_handler);
    }
}

function %prefix%_autoload_image_handler(identifier, element) {
    "use strict";
    var parts = null,
        ticketid = null,
        mimetype = null,
        filename = null;
    if (identifier == null || identifier === "") {
        %prefix%_trace("Called get_image without download identifier", null, null);
    } else {
        parts = identifier.split(";", 3);
        if (parts.length === 3) {
            ticketid = parts[0];
            mimetype = parts[1];
            filename = parts[2];
            %prefix%_api_call(
                %prefix%_get_endpoint(),
                "download",
                {
                    file_label: filename,
                    file_mime: mimetype,
                    file_name: ticketid,
                    link_mode: "direct"
                },
                function (status, response) {
                    if (status >= 200 && status < 300) {
                        element.style.backgroundRepeat = "no-repeat";
                        element.style.backgroundSize = "contain";
                        element.style.backgroundImage = "url('" + response.link + "')";
                        element.setAttribute('data-loaded-image', 'true');
                    } else {
                        %prefix%_trace("Could not download image", identifier, null);
                    }
                }
            );
        } else {
            element.style.backgroundRepeat = "no-repeat";
            element.style.backgroundSize = "contain";
            element.style.backgroundImage = "url('" + identifier + "')";
            element.setAttribute('data-loaded-image', 'true');
        }
    }
}

function %prefix%_autoload_image_batch_handler(batch) {
    "use strict";
    var parts = null,
        ticketid = null,
        mimetype = null,
        filename = null,
        i = null,
        entry_obj = null,
        dl_batch = [];
    for (i = 0; i < batch.length; i++) {
        parts = batch[i].identifier.split(";", 3);
        if (parts.length === 3) {
            ticketid = parts[0];
            mimetype = parts[1];
            filename = parts[2];
            entry_obj = {
                file_label: filename,
                file_mime: mimetype,
                file_name: ticketid,
                link_mode: "direct"
            };
            dl_batch.push(entry_obj);
        } else {
            batch[i].element.style.backgroundRepeat = "no-repeat";
            batch[i].element.style.backgroundSize = "contain";
            batch[i].element.style.backgroundImage = "url('" + batch[i].identifier + "')";
            batch[i].element.setAttribute('data-loaded-image', 'true');
        }
    }
    %prefix%_api_call(
        %prefix%_get_endpoint(),
        "download_batch",
        {
            batch: dl_batch
        },
        function (status, response) {
            if (status >= 200 && status < 300) {
                if (batch.length === response.length) {
                    for (i = 0; i < batch.length; i++) {
                        batch[i].element.style.backgroundRepeat = "no-repeat";
                        batch[i].element.style.backgroundSize = "contain";
                        batch[i].element.style.backgroundImage = "url('" + response[i].link + "')";
                        batch[i].element.setAttribute('data-loaded-image', 'true');
                    }
                } else {
                    %prefix%_trace("Mismatch during image batch download", null, null);
                }
            } else {
                %prefix%_trace("Could during image batch download", null, null);
            }
        }
    );
}

function %prefix%_autoload_image() {
    "use strict";
    var elements = null,
        i = null,
        batch = null,
        attr_id_val = null;
    elements = %prefix%_get_elements_by_tag("span");
    if (elements.length < 10) {
        for (i = 0; i < elements.length; i++) {
            if (elements[i].hasAttribute("data-identifier") && (!elements[i].hasAttribute("data-loaded-image"))) {
                %prefix%_autoload_image_handler(elements[i].getAttribute("data-identifier"), elements[i]);
            }
        }
    } else {
        batch = [];
        for (i = 0; i < elements.length; i++) {
            if (elements[i].hasAttribute("data-identifier") && (!elements[i].hasAttribute("data-loaded-image"))) {
                attr_id_val = elements[i].getAttribute("data-identifier");
                if (attr_id_val == null || attr_id_val === "") {
                    %prefix%_trace("Called get_image without download identifier", null, null);
                } else {
                    batch.push({
                        "identifier": attr_id_val,
                        "element": elements[i]
                    });
                }
            }
        }
        %prefix%_autoload_image_batch_handler(batch);
    }
}

function %prefix%_pass_form(form_el, callback) {
    "use strict";
    setTimeout(function () {
        callback(form_el);
    }, 0);
}

function %prefix%_load_form(form_el, load_params, callback) {
    "use strict";
    var parent_id = null,
        service_read = null,
        result = null,
        params = null,
        i = null,
        param = null,
        parts = null;
    if (load_params == null) {
        load_params = {};
    } else {
        parent_id = load_params.id;
    }
    service_read = form_el.getAttribute("data-service-read");
    if (service_read != null && service_read.length > 0) {
        form_el.classList.add("loading");
        //%prefix%_write_model_to_form(form_el, {});
        if (service_read === "location.hash") {
            result = {};
            params = location.hash.substr(1).split("&");
            for (i = 0; i < params.length; i++) {
                param = params[i];
                parts = param.split("=", 2);
                result[parts.shift()] = decodeURIComponent(String(parts.pop()));
            }
            %prefix%_write_model_to_form(form_el, result);
            if (typeof callback === "function") {
                callback();
            }
        } else {
            %prefix%_api_call(
                "%apiurl%",
                service_read,
                load_params,
                function (status, response) {
                    form_el.classList.remove("loading");
                    %prefix%_trace("load_form response", status, response);
                    if (response != null) {
                        if (response instanceof Array && response.length === 1) {
                            response = response[0];
                        }
                        if (parent_id != null) {
                            response.parent_id = parent_id;
                        }
                        %prefix%_write_model_to_form(form_el, response);
                    } else {
                        %prefix%_write_model_to_form(form_el, {});
                    }
                    %prefix%_autoload_image();
                    if (typeof callback === "function") {
                        callback();
                    }
                }
            );
        }
    } else {
        if (typeof callback === "function") {
            callback();
        }
    }
}

function %prefix%_load_options(sel_el, callback) {
    "use strict";
    var service = null,
        i = null,
        option = null;
    service = sel_el.getAttribute("data-bind");
    if (service != null && service.length > 0) {
        %prefix%_api_call(
            "%apiurl%",
            service,
            {},
            function (status, response) {
                var default_val = null;
                if (sel_el.hasAttribute("data-default")) {
                    default_val = sel_el.getAttribute("data-default");
                }
                %prefix%_trace("init_selects response", status, response);
                if (response != null) {
                    if (response instanceof Array) {
                        sel_el.innerHTML = "";
                        for (i = 0; i < response.length; i++) {
                            option = document.createElement("option");
                            option.setAttribute("value", response[i].id);
                            option.innerText = response[i].value;
                            sel_el.appendChild(option);
                            if (default_val === String(response[i].id) || default_val === String(response[i].value)) {
                                option.setAttribute("selected", "selected");
                            }
                        }
                    }
                }
                callback(sel_el);
            }
        );
    }
}

function %prefix%_init_selects(form_el, callback) {
    "use strict";
    var sel_els = null,
        i = null,
        options = null,
        f = null,
        ready_map = null;
    sel_els = %prefix%_get_elements_by_tag("select", form_el);
    ready_map = [];
    f = function (sel_el) {
        var j = null;
        if (ready_map != null) {
            if (ready_map.indexOf(sel_el) === -1) {
                ready_map.push(sel_el);
            }
            if (ready_map.length === sel_els.length) {
                ready_map = null;
                for (j = 0; j < sel_els.length; j++) {
                    if (sel_els[j].hasAttribute("data-readvalue")) {
                        sel_els[j].value = sel_els[j].getAttribute("data-readvalue");
                    }
                }
                callback();
            }
        }
    };
    if (sel_els.length === 0) {
        callback();
    } else {
        for (i = 0; i < sel_els.length; i++) {
            options = %prefix%_get_elements_by_tag("option", sel_els[i]);
            if (options.length === 0) {
                %prefix%_load_options(sel_els[i], f);
            } else {
                f(sel_els[i]);
            }
        }
    }
}

function %prefix%_set_list_item_selection(list_item_el) {
    "use strict";
    var i = null,
        els = null,
        rule = "list_item_selected";
    els = %prefix%_get_elements_by_tag("li", list_item_el.parentNode);
    for (i = 0; i < els.length; i++) {
        if (els[i].classList.contains(rule)) {
            els[i].classList.remove(rule);
        }
    }
    if (list_item_el != null) {
        list_item_el.classList.add(rule);
    }
}

function %prefix%_list_item_is_selected(list_item_el) {
    "use strict";
    var rule = "list_item_selected";
    return list_item_el.classList.contains(rule);
}

function %prefix%_unset_list_item_selection(list_item_el) {
    "use strict";
    var rule = "list_item_selected";
    return list_item_el.classList.remove(rule);
}

function %prefix%_select_item(list_item_el, form_el, model_map) {
    "use strict";
    if (%prefix%_list_item_is_selected(list_item_el)) {
        form_el.classList.remove("active");
        form_el.classList.add("inactive");
        // cleanup references
        form_el = null;
        model_map = null;
    } else {
        %prefix%_init_selects(form_el, function () {
            form_el.classList.remove("inactive");
            form_el.classList.add("active");
            %prefix%_load_form(form_el, {model: model_map});
            // cleanup references
            form_el = null;
            model_map = null;
        });
    }
    // cleanup references
    list_item_el = null;
}

function %prefix%_register_editor_select_toform(list_item_el, form_el, model_map) {
    "use strict";
    var f = function () {
        %prefix%_select_item(list_item_el, form_el, model_map);
    };
    %prefix%_update_listener(list_item_el, "click", f, true);
}

function %prefix%_find_last_child(el) {
    "use strict";
    var i = null;
    for (i = el.childNodes.length - 1; i >= 0; i--) {
        if (el.childNodes[i].nodeType != 3) {
            return el.childNodes[i];
        }
    }
    return null;
}

function %prefix%_write_model_to_list(list_el, model_map, list_item_id) {
    "use strict";
    var t = null,
        c = null,
        inputs = null,
        i = null,
        input_el = null,
        model = null,
        triggers = null,
        form_el = null,
        new_child = null,
        value = null,
        toggleval = null,
        index_str = "",
        index_delim = "";
    t = %prefix%_get_element_by_id("autolist-" + list_el.getAttribute("data-name"));
    c = %prefix%_import_node(t);
    inputs = %prefix%_get_elements_by_tag("span", c);
    for (i = 0; i < inputs.length; i++) {
        input_el = inputs[i];
        if (input_el.getAttribute("data-name") != null && input_el.getAttribute("data-name").length > 0) {
            model = model_map[input_el.getAttribute("data-name")];
            if (model != null) {
                value = model_map[input_el.getAttribute("data-name")];
                if (typeof value == "object" && value.hasOwnProperty("date")) {
                    input_el.textContent = (new Date(Date.parse(value.date.replace(" ", "T")))).toLocaleDateString();
                    // /* + " " + value.timezone*/
                } else if (typeof value == "number") {
                    input_el.setAttribute("data-numeric-value", value);
                    if (input_el.getAttribute("data-name").indexOf("is_") === 0) {
                        // this is a to do
                        toggleval = value;
                    }
                    input_el.textContent = value;
                } else {
                    if (input_el.hasAttribute("data-identifier")) {
                        input_el.setAttribute("data-identifier", value);
                    } else {
                        input_el.textContent = value;
                        index_str = index_str + index_delim + value;
                        index_delim = "$";
                    }
                }
            }
        }
    }
    %prefix%_get_elements_by_tag("li", c).shift().setAttribute("data-index", index_str);
    triggers = %prefix%_get_elements_by_class("autolist-trigger", c);
    for (i = 0; i < triggers.length; i++) {
        %prefix%_register_list_trigger(triggers[i], model_map, list_el.getAttribute("data-name"), list_item_id);
    }
    list_el.appendChild(c);
    new_child = %prefix%_find_last_child(list_el);
    new_child.setAttribute("id", list_item_id);
    %prefix%_clear_multi_listener(new_child, "click");
    if (toggleval !== null) {
        new_child.setAttribute("data-toggle-value", toggleval);
    }
    if (list_el.getAttribute("data-link-form") != null) {
        form_el = %prefix%_get_element_by_name(list_el.getAttribute("data-link-form"));
        %prefix%_register_editor_select_toform(new_child, form_el, model_map);
    }
    return new_child;
}

function %prefix%_get_listid(name) {
    "use strict";
    var store = null;
    if (name.charAt(0) === "*") {
        name = name.substring(1);
    }
    if (window.hasOwnProperty("%prefix%_global_lists")) {
        store = window.%prefix%_global_lists;
        if (store[name] != null) {
            return store[name].id;
        }
    }
    return null;
}

function %prefix%_make_download(content, content_type, filename) {
    "use strict";
    var byte_num = null,
        byte_ar = null,
        i = null,
        blob = null,
        el_a = null,
        target = null;
    byte_num = [];
    for (i = 0; i < content.length; i++) {
        byte_num[i] = content.charCodeAt(i);
    }
    byte_ar = new window.Uint8Array(byte_num);
    blob = new window.Blob([byte_ar], {"type": content_type});
    el_a = document.createElement("a");
    el_a.setAttribute("href", window.URL.createObjectURL(blob));
    el_a.setAttribute("download", filename);
    target = document.querySelector("body");
    target.appendChild(el_a);
    el_a.click();
    target.removeChild(el_a);
}

function %prefix%_escape_csv_text(text, text_delim, line_delim, field_delim) {
    "use strict";
    if (text.indexOf(text_delim) > -1 || text.indexOf(line_delim) > -1 || text.indexOf(field_delim) > -1) {
        return text_delim + text.replace(text_delim, text_delim + text_delim) + text_delim;
    }
    return text;
}

function %prefix%_make_export(list_el, model_list) {
    "use strict";
    var el = null,
        f = null,
        s = null,
        value = null,
        key_list = null,
        k = null,
        id = null,
        text_delim = "\"",
        line_delim = "\r\n",
        field_delim = ",",
        code = null;
    id = list_el.getAttribute("id");
    el = %prefix%_get_element_by_id(id + "-export");
    if (model_list.length > 0) {
        code = "<img src=\"style/file-icon-csv.png\" class=\"export-icon\" id=\"" + id + "-export-icon\"/>";
        code += "<div class=\"export-select\" id=\"" + id + "-export-select\" style=\"display:none;\">";
        key_list = Object.keys(model_list[0]);
        for (k = 0; k < key_list.length; k++) {
            code += "<input type='checkbox' id='" + id + "-export-" + k + "' checked/><label class='export-select-label' for='" + id + "'>" + key_list[k] + "</label>";
        }
        code += "</div>";
        code += "<button class=\"export-select-toggle\" id=\"" + id + "-export-select-toggle\">&#8811;</button>";
        el.innerHTML = code;
    } else {
        el.innerHTML = "";
    }
    s = function () {
        var button_el = null,
            select_el = null;
        select_el = %prefix%_get_element_by_id(id + "-export-select");
        button_el = %prefix%_get_element_by_id(id + "-export-select-toggle");
        if (window.getComputedStyle(select_el, null).getPropertyValue("display") == "none") {
            select_el.style.display = "inline";
            button_el.innerHTML = "&#8810";
        } else {
            select_el.style.display = "none";
            button_el.innerHTML = "&#8811";
        }
    };
    f = function () {
        var i = 0,
            csv = "",
            j = 0,
            delim = null,
            ck_el = null,
            li_list = null,
            real_idx = null,
            l = null,
            cond = null;
        delim = "";
        for (i = 0; i < key_list.length; i++) {
            ck_el = %prefix%_get_element_by_id(id + "-export-" + i);
            if (ck_el.checked) {
                csv += delim + %prefix%_escape_csv_text(key_list[i], text_delim, line_delim, field_delim);
                delim = field_delim;
            }
        }
        if (csv.length > 0) {
            csv += line_delim;
        }
        li_list = [];
        for (i = 0; i < list_el.childNodes.length; i++) {
            if (list_el.childNodes[i].nodeType != 3) {
                li_list.push(list_el.childNodes[i]);
            }
        }
        for (i = 0; i < model_list.length; i++) {
            real_idx = i;
            if (li_list[i].hasAttribute("data-original-index")) {
                for (l = 0; l < li_list.length; l++) {
                    if (Number(li_list[l].getAttribute("data-original-index")) === i) {
                        real_idx = l;
                        break;
                    }
                }
            }
            cond = (li_list.length === model_list.length) && ((li_list[real_idx].style.display === "none") || li_list[real_idx].classList.contains("item_invisible"));
            if (!cond) {
                delim = "";
                for (j = 0; j < key_list.length; j++) {
                    ck_el = %prefix%_get_element_by_id(id + "-export-" + j);
                    if (ck_el.checked) {
                        if (model_list[i].hasOwnProperty(key_list[j])) {
                            value = model_list[i][key_list[j]];
                            if (typeof value == "string" || typeof value == "boolean") {
                                csv += delim + %prefix%_escape_csv_text(value, text_delim, line_delim, field_delim);
                            } else if (typeof value == "number") {
                                csv += delim + value;
                            } else if (typeof value == "object" && (value != null) && value.hasOwnProperty("date")) {
                                value = (new Date(Date.parse(value.date.replace(" ", "T")))).toLocaleDateString();
                                csv += delim + value;
                            } else {
                                csv += delim;
                            }
                        } else {
                            csv += delim;
                        }
                        delim = field_delim;
                    }
                }
                csv += line_delim;
            } else {
                %prefix%_trace("export", "#li_list=#model_list", "false");
            }
        }
        %prefix%_make_download(csv, "text/csv", "export.csv");
    };
    el = %prefix%_get_element_by_id(id + "-export-icon");
    if (el != null) {
        %prefix%_update_listener(el, "click", f);
    }
    el = %prefix%_get_element_by_id(id + "-export-select-toggle");
    if (el != null) {
        %prefix%_update_listener(el, "click", s);
    }
}

function %prefix%_make_search(list_el, model_list) {
    "use strict";
    var el = null,
        filter_el = null,
        f = null,
        g = null,
        hasEntries = null,
        entries = null,
        j = null;
    el = %prefix%_get_element_by_id(list_el.getAttribute("id") + "-searchfield");
    if (el == null) {
        return;
    }
    filter_el = %prefix%_get_element_by_id(list_el.getAttribute("id") + "-filterfield");
    entries = list_el.childNodes;
    hasEntries = false;
    for (j = 0; j < entries.length; j++) {
        if (entries[j].nodeType != 3) {
            hasEntries = true;
            break;
        }
    }
    if (hasEntries) {
        el.style.display = 'inline-block';
        if (%prefix%_get_elements_by_tag("option", filter_el).length > 1) {
            filter_el.style.display = 'inline-block';
        }
        f = function () {
            var i = 0,
                nodes = null,
                needle = null,
                val = null,
                glob_match = null,
                cond_match = null,
                cond = null,
                k = null,
                left = null,
                op = null,
                right = null,
                is_matching = false,
                needles = false,
                count = null;
            needle = el.value;
            if (el.getAttribute("data-filter-value") != null) {
                if (needle.length > 0) {
                    needle = el.getAttribute("data-filter-value") + " " + needle;
                } else {
                    needle = el.getAttribute("data-filter-value");
                }
            }
            nodes = [];
            count = 0;
            for (i = 0; i < list_el.childNodes.length; i++) {
                if (list_el.childNodes[i].nodeType != 3) {
                    count++;
                    nodes.push(list_el.childNodes[i]);
                }
            }
            for (i = 0; i < nodes.length; i++) {
                if (nodes[i].nodeType != 3) {
                    is_matching = true;
                    needles = [];
                    if (count === model_list.length) {
                        val = model_list[i];
                        glob_match = needle.match(/([a-zA-Z-_0-9]+[!=<>]+[']?\w+[']?)|('\w+')|(\w+)/g);
                        if (glob_match != null && glob_match.length > 0) {
                            for (k = 0; k < glob_match.length; k++) {
                                cond = glob_match[k];
                                cond_match = cond.match(/([a-zA-Z-_0-9]+)([!=<>]+)[']?([\w\s\t]+)[']?/);
                                if (cond_match != null && cond_match.length > 3) {
                                    left = cond_match[1];
                                    op = cond_match[2];
                                    right = cond_match[3];
                                    if (val.hasOwnProperty(left)) {
                                        if (op === "=") {
                                            is_matching = (val[left] == right);
                                        }
                                        if (op === "!=") {
                                            is_matching = (val[left] != right);
                                        }
                                        if (op === ">") {
                                            is_matching = (val[left] > right);
                                        }
                                        if (op === "<") {
                                            is_matching = (val[left] < right);
                                        }
                                    }
                                } else {
                                    needles.push(cond);
                                }
                            }
                        }
                    } else {
                        val = null;
                    }
                    for (k = 0; k < needles.length; k++) {
                        if (nodes[i].getAttribute("data-index").toLocaleLowerCase().indexOf(needles[k].toLocaleLowerCase()) === -1) {
                            is_matching = false;
                        }
                    }
                    if (is_matching === false) {
                        if (!nodes[i].classList.contains("item_invisible")) {
                            nodes[i].classList.add("item_invisible");
                        }
                        if (nodes[i].classList.contains("item_visible")) {
                            nodes[i].classList.remove("item_visible");
                        }
                    } else {
                        if (nodes[i].classList.contains("item_invisible")) {
                            nodes[i].classList.remove("item_invisible");
                        }
                        if (!nodes[i].classList.contains("item_visible")) {
                            nodes[i].classList.add("item_visible");
                        }
                    }
                }
            }
        };
        %prefix%_update_listener(el, "input", f);
        g = function () {
            if (filter_el.value != null && filter_el.value.length > 0) {
                el.setAttribute("data-filter-value", filter_el.value);
            } else {
                el.setAttribute("data-filter-value", "");
            }
            f();
        };
        %prefix%_update_listener(filter_el, "input", g);
        %prefix%_update_listener(filter_el, "change", g);
        f();
    } else {
        el.style.display = 'none';
        filter_el.style.display = 'none';
    }
}

function %prefix%_clear_list_selection(list_el) {
    "use strict";
    var next_list_el = null,
        lists = null,
        j = null;
    %prefix%_set_list_item_selection(list_el, null);
    if (list_el.getAttribute("data-link-list") != null) {
        lists = list_el.getAttribute("data-link-list").split(",");
        for (j = 0; j < lists.length; j++) {
            next_list_el = %prefix%_get_element_by_id(%prefix%_get_listid(lists[j]));
            %prefix%_clear_element(next_list_el);
            next_list_el.classList.remove("active");
            next_list_el.classList.add("inactive");
        }
    }
}

function %prefix%_clear_linked_list(list_el) {
    "use strict";
    var lists = null,
        j = null,
        next_list_el = null,
        next_list_form_el = null;
    if (list_el.getAttribute("data-link-list") != null) {
        lists = list_el.getAttribute("data-link-list").split(",");
        for (j = 0; j < lists.length; j++) {
            next_list_el = %prefix%_get_element_by_id(%prefix%_get_listid(lists[j]));
            %prefix%_clear_linked_list(next_list_el);
        }
    }
    %prefix%_clear_element(list_el);
    list_el.classList.remove("active");
    list_el.classList.add("inactive");
    //%prefix%_load_list(next_list_els[k], {model: model_map});
    if (list_el.getAttribute("data-link-form") != null) {
        next_list_form_el = %prefix%_get_element_by_name(list_el.getAttribute("data-link-form"));
        next_list_form_el.classList.remove("active");
        next_list_form_el.classList.add("inactive");
        %prefix%_write_model_to_form(next_list_form_el, {
            id: null,
            parent_id: null
        });
    }
}

function %prefix%_load_list_linked_forms(list_el, parent_id) {
    "use strict";
    var form_el = null;
    if (list_el.getAttribute("data-link-form") != null) {
        form_el = %prefix%_get_element_by_name(list_el.getAttribute("data-link-form"));
        form_el.classList.remove("active");
        form_el.classList.add("inactive");
        list_el.classList.remove("loaded_linked_forms");
        console.log("load linked forms", list_el);
        if (parent_id != null) {
            %prefix%_get_formoriginal(form_el.getAttribute("name")).parent_id = parent_id;
            %prefix%_init_selects(form_el, function () {
                %prefix%_write_model_to_form(form_el, {
                    parent_id: parent_id
                });
                list_el.classList.add("loaded_linked_forms");
                console.log("loaded linked forms", list_el);
            });
        }
    }
}

function %prefix%_pass_list(list_el, load_params, callback) {
    "use strict";
    var parent_id = null;
    if (load_params == null) {
        if (list_el.hasAttribute("parentid")) {
            parent_id = list_el.getAttribute("parentid");
        }
    } else {
        parent_id = load_params.model.id;
        list_el.setAttribute("parentid", parent_id);
    }
    %prefix%_call_gui_hooks("pass_list_start", list_el, load_params);
    setTimeout(function () {
        %prefix%_load_list_linked_forms(list_el, parent_id);
        callback(list_el);
        %prefix%_call_gui_hooks("pass_list_end", list_el, load_params);
    }, 0);
}

function %prefix%_load_list(list_el, load_params, callback) {
    "use strict";
    var service_list = null,
        parent_id = null,
        next_list_el = null,
        list_item = null,
        addendum = "";
    service_list = list_el.getAttribute("data-service-list");
    %prefix%_call_gui_hooks("load_list_start", list_el, load_params);
    console.log("start list loading", list_el);
    if (load_params == null) {
        load_params = {};
        if (list_el.hasAttribute("parentid")) {
            parent_id = list_el.getAttribute("parentid");
            load_params.model = {};
            load_params.model.id = parent_id;
        }
    } else {
        parent_id = load_params.model.id;
        list_el.setAttribute("parentid", parent_id);
    }
    if (service_list != null && service_list.length > 0 && !list_el.classList.contains("loading")) {
        list_el.classList.add("loading");
        %prefix%_api_call(
            "%apiurl%",
            service_list,
            load_params,
            function (status, response) {
                var i = null,
                    f = null,
                    c = null,
                    lists = null,
                    j = null,
                    list_item_id = null,
                    next_list_els = null;
                %prefix%_trace("load_list response", status, response);
                f = function (list_item_el, f_next_list_els, model_map) {
                    return function () {
                        var k = 0;
                        if (list_item_el != null) {
                            if (%prefix%_list_item_is_selected(list_item_el)) {
                                %prefix%_unset_list_item_selection(list_item_el);
                                for (k = 0; k < f_next_list_els.length; k++) {
                                    %prefix%_clear_linked_list(f_next_list_els[k]);
                                }
                            } else {
                                %prefix%_set_list_item_selection(list_item_el);
                                for (k = 0; k < f_next_list_els.length; k++) {
                                    %prefix%_clear_element(f_next_list_els[k]);
                                    f_next_list_els[k].classList.remove("inactive");
                                    f_next_list_els[k].classList.add("active");
                                    %prefix%_set_list_params(f_next_list_els[k].getAttribute("data-name"), {model: model_map});
                                    %prefix%_load_list(f_next_list_els[k], {model: model_map});
                                }
                            }
                        }
                    };
                };
                %prefix%_load_list_linked_forms(list_el, parent_id);
                list_el.classList.remove("loading");
                if (response != null) {
                    %prefix%_clear_element(list_el);
                    for (i = 0; i < response.length; i++) {
                        if (parent_id != null) {
                            response[i].parent_id = parent_id;
                        }
                        if (response[i].hasOwnProperty("id")) {
                            addendum = "-" + String(response[i].id);
                        }
                        list_item_id = list_el.getAttribute("id") + "-" + String(i) + addendum;
                        list_item = %prefix%_write_model_to_list(list_el, response[i], list_item_id);
                        next_list_els = [];
                        if (list_el.getAttribute("data-link-list") != null) {
                            lists = list_el.getAttribute("data-link-list").split(",");
                            for (j = 0; j < lists.length; j++) {
                                if (lists[j].charAt(0) !== "*") {
                                    next_list_el = %prefix%_get_element_by_id(%prefix%_get_listid(lists[j]));
                                    next_list_el.classList.remove("active");
                                    next_list_el.classList.add("inactive");
                                    next_list_els.push(next_list_el);
                                }
                            }
                        }
                        c = f(list_item, next_list_els, response[i]);
                        %prefix%_update_listener(list_item, "click", c, true);
                    }
                    %prefix%_make_export(list_el, response);
                    %prefix%_make_search(list_el, response);
                    %prefix%_register_list_multiselect_triggers(list_el, response);
                    %prefix%_autoload_image();
                }
                if (typeof callback === "function") {
                    callback();
                }
                console.log("list loading completed", list_el);
                %prefix%_call_gui_hooks("load_list_end", list_el, load_params);
            }
        );
    } else {
        if (typeof callback === "function") {
            callback();
        }
        %prefix%_call_gui_hooks("load_list_end", list_el, load_params);
    }
}

function %prefix%_upload_api_call(api_method, parameters, callback) {
    "use strict";
    %prefix%_api_call(
        "%apiurl%",
        api_method,
        parameters || {},
        function (status, response) {
            console.debug("upload_api_call response", status, response);
            if (response != null) {
                callback(response);
            }
        }
    );
}

function %prefix%_filereader(blob, success_handler, error_handler) {
    "use strict";
    var filereader = new window.FileReader();
    %prefix%_call_gui_hooks("filereader_start", filereader, blob);
    filereader.onerror = function (event) {
        error_handler(filereader, event);
        %prefix%_call_gui_hooks("filereader_error", event, blob);
    };
    filereader.onloadend = function (event) {
        success_handler(filereader, event);
        %prefix%_call_gui_hooks("filereader_end", event, blob);
    };
    filereader.readAsDataURL(blob);
}

function %prefix%_dataurl_decompose(dataurl) {
    "use strict";
    var parts = null,
        rest = null,
        splitted_parts = null,
        descriptor = null,
        data = null,
        splitted_desc = null,
        parts_mime = null,
        i = null,
        part = null,
        parts_charset = null,
        mimetype = null;
    parts = dataurl.split(":");
    rest = parts.slice(1).join(":");
    splitted_parts = rest.split(",");
    descriptor = splitted_parts[0];
    data = splitted_parts[1];
    splitted_desc = descriptor.split(";");
    parts_mime = [];
    for (i = 0; i < splitted_desc.length; i++) {
        part = splitted_desc[i];
        if (part.indexOf("charset") === 0) {
            parts_charset = part.split("=");
            parts_mime.push(parts_charset[1]);
        } else if (part.indexOf("base64") !== 0) {
            parts_mime.push(part);
        }
    }
    mimetype = parts_mime.join(";");
    return {data: data, mimetype: mimetype};
}

function %prefix%_upload(file_blob, finish, error) {
    "use strict";
    %prefix%_upload_api_call("begin_upload", {}, function (response) {
        var ticket_id = null,
            size = null,
            slices = null,
            p = null;
        ticket_id = response.ticket_id;
        size = response.size;
        slices = [];
        for (p = 0; p < file_blob.size; p += size) {
            slices.push(file_blob.slice(p, p + size));
        }
        %prefix%_call_gui_hooks("upload_start", ticket_id, slices.length);
        (function iterate(i) {
            var slice = null;
            if (i < slices.length) {
                slice = slices[i];
                %prefix%_filereader(slice, function (filereader) {
                    var dataurl = filereader.result,
                        content = %prefix%_dataurl_decompose(dataurl),
                        payload = {
                            "index": i,
                            "mime": file_blob.type,
                            "slice": content.data,
                            "ticket_id": ticket_id
                        };
                    %prefix%_call_gui_hooks("upload_progress", filereader, payload);
                    %prefix%_upload_api_call("continue_upload", payload, function () {
                        iterate(i + 1);
                    });
                }, function (filereader, event) {
                    error();
                    %prefix%_trace("upload error", filereader, event, null);
                    %prefix%_call_gui_hooks("upload_error", event, i);
                });
            } else {
                %prefix%_upload_api_call("finish_upload", {"ticket_id": ticket_id}, function () {
                    var payload = {
                        "filename": file_blob.name,
                        "mimetype": file_blob.type,
                        "ticket_id": ticket_id
                    };
                    finish(payload);
                    %prefix%_call_gui_hooks("upload_end", ticket_id, payload);
                });
            }
        }(0));
    });
}

function %prefix%_upload_files(form_el, model_map, finish) {
    "use strict";
    var file_els = null;
    console.debug("upload_files", model_map);
    file_els = %prefix%_get_elements_by_tag("input", form_el);
    (function iterate(i) {
        var file_el = null;
        if (i < file_els.length) {
            file_el = file_els[i];
            if (file_el.type === "file") {
                if (file_el.files.length > 0) {
                    %prefix%_upload(file_el.files[0], function (result) {
                        if (file_el.name.length > 0) {
                            model_map[file_el.name] = result.ticket_id + ";" + result.mimetype + ";" + result.filename;
                        }
                        iterate(i + 1);
                    }, function () {
                        console.debug("unexpected state");
                    });
                } else {
                    iterate(i + 1);
                }
            } else {
                iterate(i + 1);
            }
        } else {
            finish();
        }
    }(0));
}

function %prefix%_submit_form(e, callback) {
    "use strict";
    var el_name = null,
        form_el = null,
        model_map = null;
    e.preventDefault();
    el_name = e.target.getAttribute("name");
    form_el = %prefix%_get_element_by_name(el_name);
    model_map = %prefix%_get_model_from_form(form_el);
    %prefix%_call_gui_hooks("submit_form_start", form_el, model_map);
    %prefix%_upload_files(form_el, model_map, function () {
        console.debug("submit_form model", model_map);
        %prefix%_api_call(
            "%apiurl%",
            form_el.getAttribute("data-service-write"),
            {model: model_map},
            function (status, response) {
                console.debug("submit_form response", status, response);
                if (!model_map.hasOwnProperty("id")) {
                    model_map.id = response;
                    %prefix%_write_model_to_form(form_el, model_map);
                }
                if (form_el.hasAttribute("data-delegate-target")) {
                    location.href =
                        location.href.substr(0, location.href.lastIndexOf("/")) +
                        "/" +
                        form_el.getAttribute("data-delegate-target");
                }
                callback(status, response);
                %prefix%_call_gui_hooks("submit_form_end", form_el, {
                    response: response,
                    status: status
                });
            }
        );
    });
    return false;
}

function %prefix%_init_richtext(id) {
    "use strict";
    var el = null,
        ref = null,
        par = null,
        editor = null,
        editor_actions = null,
        interval_id = null,
        f = null,
        g = null;
    el = document.createElement("div");
    ref = %prefix%_get_element_by_id(id);
    el.setAttribute("data-for", id);
    el.setAttribute("data-name", ref.getAttribute("name"));
    el.setAttribute("class", "editorwrapper");
    par = ref.parentNode;
    // par.insertBefore(el, ref);
    par.appendChild(el);
    ref.style.width = 0;
    ref.style.height = 0;
    ref.style.padding = 0;
    editor_actions = window.hasOwnProperty("%prefix%_richtext_options") ? window.%prefix%_richtext_options : [
        "bold",
        "italic",
        "underline",
        "paragraph",
        "olist",
        "ulist",
        {
            icon: ">",
            name: "indent",
            result: function () {
                window.pell.exec("indent");
            },
            title: "Indent"
        },
        {
            icon: "<",
            name: "outdent",
            result: function () {
                window.pell.exec("outdent");
            },
            title: "Outdent"
        }
    ];
    g = function (html) {
        if (ref.getAttribute("data-fromeditor") != html) {
            ref.value = html;
            ref.setAttribute("data-fromeditor", html);
        }
    };
    editor = window.pell.init({
        actions: editor_actions,
        element: el,
        onChange: g
    });
    if (window.hasOwnProperty("%prefix%_richtext_paste_interceptor")) {
        f = function (event) {
            window.%prefix%_richtext_paste_interceptor(event);
            g(editor.content.innerHTML);
        };
        %prefix%_update_listener(el, "paste", f);
    }
    if (ref.hasAttribute("data-intervalid")) {
        clearInterval(Number(ref.getAttribute("data-intervalid")));
    }
    interval_id = setInterval(function () {
        if (ref.value != ref.getAttribute("data-fromeditor")) {
            editor.content.innerHTML = ref.value;
            ref.setAttribute("data-fromeditor", ref.value);
        }
    }, 300);
    ref.setAttribute("data-intervalid", String(interval_id));
}

function %prefix%_register_form_plugins(el) {
    "use strict";
    var i = null,
        inputs = %prefix%_get_elements_by_tag("input", el),
        c = null;
    c = function (e) {
        window[e.target.getAttribute("data-check")](e.target);
    };
    for (i = 0; i < inputs.length; i++) {
        if (inputs[i].hasAttribute("data-check")) {
            %prefix%_update_listener(inputs[i], "input", c);
        }
        if (inputs[i].hasAttribute("data-mode") && (inputs[i].getAttribute("data-mode") === "richtext")) {
            %prefix%_init_richtext(inputs[i].id);
        }
        if (inputs[i].hasAttribute("data-mode") && (inputs[i].getAttribute("data-mode").indexOf("widget:") === 0)) {
            if (window.hasOwnProperty(inputs[i].getAttribute("data-mode").substr(7))) {
                window[inputs[i].getAttribute("data-mode").substr(7)](inputs[i]);
            } else {
                %prefix%_trace("register_form_plugins", "widget initializer not found", inputs[i].getAttribute("data-mode"));
            }
        }
    }
}

function %prefix%_trigger_submit(e, callback) {
    "use strict";
    e.target.classList.add("submitting");
    %prefix%_submit_form(e, function (status, response) {
        var formlinks = %prefix%_get_formlinks(e.target.getAttribute("name")),
            i = null;
        for (i = 0; i < formlinks.length; i++) {
            formlinks[i].clear();
            formlinks[i].load();
        }
        if (e.target.hasAttribute("data-feedback")) {
            if (e.target.getAttribute("data-feedback") === "sticker") {
                if (status === 200) {
                    e.target.textContent = "Success ✓";
                } else {
                    if (status === 424) {
                        e.target.textContent = "Failure ✗: " + String(response);
                    } else {
                        e.target.textContent = "Failure ✗";
                    }
                }
            }
        }
        e.target.classList.remove("submitting");
        if (typeof callback == "function") {
            callback(response);
        }
    });
}

function %prefix%_register_submit_handler(el) {
    "use strict";
    %prefix%_update_listener(el, "submit", %prefix%_trigger_submit);
}

function %prefix%_register_form(name) {
    "use strict";
    var store = null;
    if (!window.hasOwnProperty("%prefix%_global_forms")) {
        window.%prefix%_global_forms = {};
    }
    store = window.%prefix%_global_forms;
    if (!store[name]) {
        store[name] = {
            links: [],
            original: %prefix%_get_model_from_form(%prefix%_get_element_by_name(name))
        };
    }
}

function %prefix%_register_list(name, id) {
    "use strict";
    var store = null;
    if (!window.hasOwnProperty("%prefix%_global_lists")) {
        window.%prefix%_global_lists = {};
    }
    store = window.%prefix%_global_lists;
    if (!store[name]) {
        store[name] = {};
        store[name].id = id;
        store[name].load_params = null;
        store[name].load = function () {
            %prefix%_clear_element(%prefix%_get_element_by_id(id));
            %prefix%_load_list(%prefix%_get_element_by_id(id), store[name].load_params);
        };
    }
}

function %prefix%_register_formlinks(el) {
    "use strict";
    var store = null,
        name = null,
        i = null,
        j = null,
        lists = null;
    name = el.getAttribute("data-link-form");
    if (name != null) {
        if (window.hasOwnProperty("%prefix%_global_forms")) {
            store = %prefix%_get_formlinks(name);
            if (store != null) {
                for (i = 0; i < store.length; i++) {
                    if (store[i].name === el.getAttribute("data-name")) {
                        return;
                    }
                }
                store.push({
                    clear: function () {
                        var list = %prefix%_get_element_by_id(%prefix%_get_listid(el.getAttribute("data-name")));
                        %prefix%_clear_element(list);
                        if (list.getAttribute("data-link-list") != null) {
                            lists = list.getAttribute("data-link-list").split(",");
                            for (j = 0; j < lists.length; j++) {
                                %prefix%_clear_element(%prefix%_get_element_by_id(%prefix%_get_listid(lists[j])));
                                %prefix%_load_list(%prefix%_get_element_by_id(%prefix%_get_listid(lists[j])), {model: {}});
                            }
                        }
                    },
                    load: function () {
                        var list = %prefix%_get_element_by_id(%prefix%_get_listid(el.getAttribute("data-name"))),
                            list_name = null;
                        %prefix%_load_list(list, %prefix%_get_list_params(el.getAttribute("data-name")));
                        if (list.getAttribute("data-link-list") != null) {
                            lists = list.getAttribute("data-link-list").split(",");
                            for (j = 0; j < lists.length; j++) {
                                list_name = (lists[j].charAt(0) === "*") ? lists[j].substring(1) : lists[j];
                                %prefix%_load_list(%prefix%_get_element_by_id(%prefix%_get_listid(lists[j])), %prefix%_get_list_params(list_name));
                            }
                        }
                    },
                    name: el.getAttribute("data-name"),
                    reset: function () {
                        var list = %prefix%_get_element_by_id(%prefix%_get_listid(el.getAttribute("data-name")));
                        %prefix%_clear_list_selection(list);
                    }
                });
            }
        }
    }
}

function %prefix%_get_session(callback) {
    "use strict";
    %prefix%_api_call(
        "%apiurl%",
        "session",
        {},
        function (status, response) {
            var elements = null,
                i = null;
            console.log("session", status, response);
            if (response != null && response.id != null) {
                elements = %prefix%_get_elements_by_class("autonav");
                for (i = 0; i < elements.length; i++) {
                    elements[i].setAttribute("data-current-role", response.role);
                }
            } else {
                elements = %prefix%_get_elements_by_class("autonav");
                for (i = 0; i < elements.length; i++) {
                    elements[i].setAttribute("data-current-role", "guest");
                }
            }
            if (typeof callback === "function") {
                callback();
            }
        }
    );
}

function %prefix%_register_nav_handlers(nav_el) {
    "use strict";
    var items = null,
        i = null,
        link = null,
        item = null,
        f = null;
    f = function (navlink) {
        return function () {
            var href = null,
                len = null,
                prefix = null,
                url = null;
            if (navlink != null) {
                if ((navlink.indexOf("/") === 0) || /^[a-zA-Z]+:\/\//.test(navlink)) {
                    url = navlink;
                    window.open(url, "_blank");
                } else {
                    href = window.location.href;
                    len = href.split("/").pop().length;
                    prefix = href.substring(0, href.length - len);
                    url = prefix + navlink;
                    window.location.href = url;
                }
            }
        };
    };
    items = %prefix%_get_elements_by_tag("li", nav_el);
    for (i = 0; i < items.length; i++) {
        item = items[i];
        link = item.getAttribute("data-link");
        item.addEventListener("click", f(link));
    }
}

function %prefix%_get_all_form_els(form_el) {
    "use strict";
    var label_els = null,
        input_els = null,
        select_els = null,
        div_els = null,
        span_els = null;
    label_els = [].slice.call(%prefix%_get_elements_by_tag("label", form_el));
    input_els = [].slice.call(%prefix%_get_elements_by_tag("input", form_el));
    select_els = [].slice.call(%prefix%_get_elements_by_tag("select", form_el));
    div_els = [].slice.call(%prefix%_get_elements_by_tag("div", form_el));
    span_els = [].slice.call(%prefix%_get_elements_by_tag("span", form_el));
    return [].concat(label_els).concat(input_els).concat(select_els).concat(div_els).concat(span_els);
}

function %prefix%_parse_condition(elmap, el, cond) {
    "use strict";
    var match = cond.match(/@?[a-zA-Z\-_0-9]+|[=!<>~]+|@?'[\w\s\t\(\)\[\]\{\}\-_]+'|[\w]+/g), // /([a-zA-Z-_0-9]+)([!=<>]+)[']?([\w\s\t]+)[']?/
        parse_step = "left",
        i = null,
        j = null,
        left = null,
        right = null,
        op = null,
        field = "id",
        elmap_entry = null;
    for (i = 0; i < match.length; i++) {
        if (parse_step == "left") {
            left = match[i];
            parse_step = "op";
        } else if (parse_step == "op") {
            op = match[i];
            parse_step = "right";
        } else if (parse_step == "right") {
            if (match[i].indexOf("'") === 0) {
                right = match[i].substring(1, match[i].length - 1);
            } else if (match[i].indexOf("@'") === 0) {
                right = match[i].substring(2, match[i].length - 1);
                field = "value";
            } else {
                right = match[i];
            }
            parse_step = "left";
            if (elmap.hasOwnProperty(left)) {
                elmap_entry = null;
                for (j = 0; j < elmap[left].length; j++) {
                    if (elmap[left][j].op === op) {
                        elmap_entry = elmap[left][j];
                        break;
                    }
                }
                if (elmap_entry === null) {
                    elmap_entry = {"els": [], "op": op, "val": []};
                    elmap[left].push(elmap_entry);
                }
                if (elmap_entry.els.indexOf(el) === -1) {
                    elmap_entry.els.push(el);
                }
                if (elmap_entry.val.indexOf(right) === -1) {
                    elmap_entry.val.push([right, field]);
                }
            } else {
                elmap[left] = [{"els": [el], "op": op, "val": [[right, field]]}];
            }
        }
    }
    // cleanup references
    match = null;
    el = null;
}

function %prefix%_fill_condition_map(all_els, el_map) {
    "use strict";
    var i = null,
        el = null;
    for (i = 0; i < all_els.length; i++) {
        el = all_els[i];
        if (el.hasAttribute("data-if")) {
            %prefix%_parse_condition(
                el_map,
                el,
                el.getAttribute("data-if")
            );
        }
    }
    // cleanup references
    all_els = null;
    el = null;
}

function %prefix%_get_input_value(input_el) {
    "use strict";
    var target_value = null,
        i = null;
    if (input_el.type == "checkbox") {
        target_value = [input_el.checked ? "true" : "false"];
    } else {
        if (input_el.tagName === "SELECT") {
            target_value = [input_el.value];
            for (i = 0; i < input_el.childNodes.length; i++) {
                if (input_el.childNodes[i].tagName === "OPTION") {
                    if (input_el.childNodes[i].value === input_el.value) {
                        target_value.push(
                            input_el.childNodes[i].textContent
                        );
                        break;
                    }
                }
            }
        } else {
            target_value = [input_el.value];
        }
    }
    return target_value;
}

function %prefix%_is_show_on_condition(struct_cond, target_values) {
    "use strict";
    var show = false,
        i = 0,
        j = 0;
    for (i = 0; i < target_values.length; i++) {
        for (j = 0; j < struct_cond.val.length; j++) {
            if (!(i === 0 && struct_cond.val[j][1] === "value")) {
                if (struct_cond.op === "=") {
                    if (struct_cond.val[j][0] === target_values[i]) {
                        show = true;
                        break;
                    }
                }
                if (struct_cond.op === "!=") {
                    if (struct_cond.val[j][0] !== target_values[i]) {
                        show = true;
                        break;
                    }
                }
                if (struct_cond.op === "<") {
                    if (struct_cond.val[j][0] < target_values[i]) {
                        show = true;
                        break;
                    }
                }
                if (struct_cond.op === ">") {
                    if (struct_cond.val[j][0] > target_values[i]) {
                        show = true;
                        break;
                    }
                }
                if (struct_cond.op === "~") {
                    if (target_values[i].indexOf(struct_cond.val[j][0]) > -1) {
                        show = true;
                        break;
                    }
                }
                if (struct_cond.op === "!~") {
                    if (target_values[i].indexOf(struct_cond.val[j][0]) === -1) {
                        show = true;
                        break;
                    }
                }
            }
        }
    }
    // cleanup references
    struct_cond = null;
    return show;
}

function %prefix%_show_on_condition(el, show) {
    "use strict";
    if (show) {
        if (el.classList.contains("invisible")) {
            el.classList.remove("invisible");
        }
        if (el.parentNode.tagName === "DIV") {
            if (el.parentNode.classList.contains("invisible")) {
                el.parentNode.classList.remove("invisible");
            }
        }
    } else {
        if (!el.classList.contains("invisible")) {
            el.classList.add("invisible");
        }
        if (el.parentNode.tagName === "DIV") {
            if (!el.parentNode.classList.contains("invisible")) {
                el.parentNode.classList.add("invisible");
            }
        }
    }
    %prefix%_call_gui_hooks("show_on_condition", el, show);
    // cleanup references
    el = null;
}

function %prefix%_make_condition_handler(el_map) {
    "use strict";
    return function (event) {
        var attr = null,
            show = null,
            i = null,
            j = null,
            target_value = null;
        if (event.target.hasAttribute("name")) {
            show = true;
            attr = event.target.getAttribute("name");
            target_value = %prefix%_get_input_value(event.target);
            %prefix%_call_gui_hooks("condition_handler", event.target, target_value);
            if (el_map.hasOwnProperty(attr)) {
                for (j = 0; j < el_map[attr].length; j++) {
                    show = %prefix%_is_show_on_condition(
                        el_map[attr][j],
                        target_value
                    );
                    for (i = 0; i < el_map[attr][j].els.length; i++) {
                        %prefix%_show_on_condition(
                            el_map[attr][j].els[i],
                            show
                        );
                    }
                }
            }
        }
        // cleanup references
        event = null;
    };
}

function %prefix%_trigger_condition_handler(form_el, handler) {
    "use strict";
    var all_els = null,
        i = null;
    all_els = %prefix%_get_all_form_els(form_el);
    for (i = 0; i < all_els.length; i++) {
        handler({
            target: all_els[i]
        });
    }
    // cleanup references
    form_el = null;
    all_els = null;
    handler = null;
}

function %prefix%_register_condition_handlers(form_el) {
    "use strict";
    var all_els = null,
        i = null,
        elmap = null,
        f = null;
    all_els = %prefix%_get_all_form_els(form_el);
    elmap = {};
    %prefix%_fill_condition_map(all_els, elmap);
    f = %prefix%_make_condition_handler(elmap);
    for (i = 0; i < all_els.length; i++) {
        f({
            target: all_els[i]
        });
    }
    %prefix%_update_listener(form_el, "input", f);
    // cleanup references
    form_el = null;
    all_els = null;
    return f;
}

function %prefix%_init(callback, ctx) {
    "use strict";
    var elements = null,
        list_elements = null,
        form_elements = null,
        i = null,
        f = null,
        cb_map = null,
        check_cbmap = null;
    cb_map = {};
    %prefix%_call_gui_hooks("init_start", "%prefix%", cb_map);
    console.log("application %prefix% starting up");
    if (window.%prefix%_debug_mode !== true) {
        console.debug("debug mode is off", "to enable: set %prefix%_debug_mode to true");
    } else {
        console.debug("debug mode is on");
    }
    check_cbmap = function () {
        var cb_keys = null,
            j = null,
            check = null;
        check = true;
        cb_keys = Object.keys(cb_map);
        for (j = 0; j < cb_keys.length; j++) {
            if (cb_map[cb_keys[j]] === false) {
                check = false;
                break;
            }
        }
        if (check) {
            console.log("application %prefix% ready");
            if (typeof callback === "function") {
                callback(ctx);
            }
            %prefix%_call_gui_hooks("init_end", "%prefix%", cb_map);
        }
    };
    elements = %prefix%_get_elements_by_class("autonav");
    for (i = 0; i < elements.length; i++) {
        %prefix%_register_nav_handlers(elements[i]);
    }
    cb_map.all = false;
    cb_map.session = false;
    /*
    %prefix%_get_session(function () {
        cb_map.session = true;
        check_cbmap();
    });*/
    cb_map.session = true;
    //check_cbmap();
    form_elements = %prefix%_get_elements_by_class("autoform");
    list_elements = %prefix%_get_elements_by_class("autolist");
    for (i = 0; i < form_elements.length; i++) {
        cb_map[form_elements[i].getAttribute("id")] = false;
    }
    for (i = 0; i < list_elements.length; i++) {
        cb_map[list_elements[i].getAttribute("id")] = false;
    }
    f = function (el) {
        return function () {
            var condition_handler = null;
            if (el.hasAttribute("data-await") && el.getAttribute("data-await") === "parent") {
                window.setTimeout(function () {
                    %prefix%_pass_form(el, function () {
                        %prefix%_trigger_condition_handler(el, condition_handler);
                        cb_map[el.getAttribute("id")] = true;
                        check_cbmap();
                    });
                }, 0);
            } else {
                window.setTimeout(function () {
                    %prefix%_load_form(el, null, function () {
                        %prefix%_trigger_condition_handler(el, condition_handler);
                        cb_map[el.getAttribute("id")] = true;
                        check_cbmap();
                    });
                }, 0);
            }
            condition_handler = %prefix%_register_condition_handlers(el);
        };
    };
    for (i = 0; i < form_elements.length; i++) {
        %prefix%_register_submit_handler(form_elements[i]);
        %prefix%_register_form_plugins(form_elements[i]);
        %prefix%_init_selects(form_elements[i], f(form_elements[i]));
        %prefix%_register_form(form_elements[i].getAttribute("name"));
    }
    f = function (el) {
        return function () {
            cb_map[el.getAttribute("id")] = true;
            check_cbmap();
        };
    };
    for (i = 0; i < list_elements.length; i++) {
        if (list_elements[i].hasAttribute("data-await") && list_elements[i].getAttribute("data-await") === "parent") {
            %prefix%_pass_list(list_elements[i], null, f(list_elements[i]));
        } else {
            %prefix%_load_list(list_elements[i], null, f(list_elements[i]));
        }
        %prefix%_register_list(list_elements[i].getAttribute("data-name"), list_elements[i].getAttribute("id"));
        %prefix%_register_formlinks(list_elements[i]);
    }
    cb_map.all = true;
    check_cbmap();
}
