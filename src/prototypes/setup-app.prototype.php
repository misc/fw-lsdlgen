<?php
  if ($argc<2) {
    echo "This script will populate public files to www root.".PHP_EOL;
    echo "Usage: [script] [OPTIONS]".PHP_EOL;
    echo "Available Options: [absolute-www-root-path] [copy]".PHP_EOL;
    echo "Files will be symlinked unless copy option is specified.".PHP_EOL;
    echo "Copy mode currently only works with POSIX.".PHP_EOL;
    echo "You may need to re-run if conflicting files are detected on first run.".PHP_EOL;
    echo "Please note that the configuration will always be symlinked.".PHP_EOL;
    die();
  }

  @$file = fopen(__DIR__.DIRECTORY_SEPARATOR."installed.files", "r");
  if ($file != FALSE) {
    echo "[ removing already installed public files ... ]".PHP_EOL;
    while (($data = fgetcsv($file)) !== FALSE) {
      $file_entry = $data[0];
      $success = @unlink($file_entry);
      if (!$success) {
        $success = !@exec("rm -rf ".$file_entry);
      }
      echo "removing $file_entry ... ".($success ? "OK" : "ERROR").PHP_EOL;
    }
    unlink(__DIR__.DIRECTORY_SEPARATOR."installed.files");
  }
  @fclose($file);

  if ($argc>1) {
    $file_list = [];

    // Copy all files from public to target

    $copy_mode = false;
    if ($argc>2) {
      $copy_mode = ($argv[2] == 'copy') ? true : false;
    }

    if ($copy_mode) {
      echo "[ copying public files ... ]".PHP_EOL;
    }
    else {
      echo "[ symlinking public files ... ]".PHP_EOL;
    }

    foreach (
      glob(
        __DIR__.DIRECTORY_SEPARATOR.
        "..".DIRECTORY_SEPARATOR.
        "public".DIRECTORY_SEPARATOR.
        "*"
      ) as $file
    ) {
      if ($file != "." && $file !== "..") {
        if ($copy_mode) {
          $success = !@exec("cp -r ".$file." ".$argv[1].DIRECTORY_SEPARATOR.basename($file));
          echo "copying $file ... ".($success ? "OK" : "ERROR").PHP_EOL;
        }
        else {
          $success = @symlink($file, $argv[1].DIRECTORY_SEPARATOR.basename($file));
          echo "symlinking $file ... ".($success ? "OK" : "ERROR").PHP_EOL;
        }
        array_push($file_list, $argv[1].DIRECTORY_SEPARATOR.basename($file));
      }
    }

    // copy conf.json to target/../setup

    echo "[ symlinking configuration ... ]".PHP_EOL;

    $target_dir =
      $argv[1].DIRECTORY_SEPARATOR.
      "..".DIRECTORY_SEPARATOR.
      "setup".DIRECTORY_SEPARATOR;
    @mkdir($target_dir);
    @unlink(
      __DIR__.DIRECTORY_SEPARATOR.
      "..".DIRECTORY_SEPARATOR.
      "setup".DIRECTORY_SEPARATOR.
      "conf.json",
      $target_dir."conf.json"
    );
    @symlink(
      __DIR__.DIRECTORY_SEPARATOR.
      "..".DIRECTORY_SEPARATOR.
      "setup".DIRECTORY_SEPARATOR.
      "conf.json",
      $target_dir."conf.json"
    );
    array_push($file_list, $target_dir."conf.json");

    // save list of copied files

    echo "[ save state of installed files ... ]".PHP_EOL;

    $file = fopen(__DIR__.DIRECTORY_SEPARATOR."installed.files", "w");
    foreach ($file_list as $file_entry) {
      fputcsv($file, array($file_entry));
    }
    fclose($file);
  }
?>
