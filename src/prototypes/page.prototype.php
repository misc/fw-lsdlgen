<?php
    namespace %vendor%\server\%prefix%;

%requires%
    use greenscale\server\auth\ACLManager;
    use greenscale\server\Config;
    use greenscale\server\router\Route;
    use greenscale\server\io\Input;
    use greenscale\server\io\Template;

    /**
     * Class %Page%Page loads and displays %Page% page
     * @author          Martin Springwald <springwald@greenscale.de>
     * @license         Greenscale Open Source License
     */
    class %Page%Page {
        private static $page_plugins = array();

        private $template_engine = null;

        /**
         * Constructor of class %Page%Page adds route
         * @param       \Router $router Router
         */
        function __construct ($router) {
            $router->add(new Route("GET", "/%page%", $this->handleRequest()));
            $this->template_engine = new Template();
        }

        /**
         * Register plugin
         * @param       function $plugin
         */
        public static function register_page_plugin ($plugin) {
            array_push(self::$page_plugins, $plugin);
        }

        /**
         * Send cache headers
         * @TODO: deal with global undocumented HTTP_IF_MODIFIED_SINCE and HTTP_IF_NONE_MATCH
         */
        private function sendCacheHeaders ($ttl) {
          if (! headers_sent()) {
            $last_mod = intval(time() / $ttl) * $ttl;
            $ehash = md5("%page%");
            $etag = 'W/' . "\"" . $ehash . "\"";
            header('Last-Modified: ' . gmdate("D, d M Y H:i:s", $last_mod). " GMT");
            header('Etag: ' . $etag);
            session_cache_expire(intval($ttl / 60));
            session_cache_limiter("public");
            $if_modified_since = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? intval($_SERVER['HTTP_IF_MODIFIED_SINCE']) : time();
            $if_none_match = isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : null;
            if (($if_none_match === $etag) || ($if_none_match === $ehash) || ($if_none_match === "\"" . $ehash . "\"")) {
              if ($if_modified_since <= $last_mod) {
                header('HTTP/1.1 304 Not Modified', true, 304);
                exit();
              }
            }
          }
        }

        /**
         * Handle request
         * @return      function
         */
        private function handleRequest () {
            return function () {
                $template = $this->template_engine->load_html_template_from_file("page_%page%");
                $vars = (object) get_defined_constants();
                $vars->path_info = Input::get_path();
                $vars->pagename = "%page%";
                %commentcache%$this->sendCacheHeaders(500);
                session_start();
                $user = ACLManager::get_current_user();
                $vars->userrole = (($user != null) && isset($user->role)) ? $user->role : "guest";
                $vars->username = (($user != null) && isset($user->name)) ? $user->name : "";
                $vars->useremail = (($user != null) && isset($user->email)) ? $user->email : "";
                if (!isset($_SESSION) || !isset($_SESSION['lang'])) {
                    $vars->language = Config::get()->main->language;
                }
                else {
                    $vars->language = $_SESSION['lang'];
                }
                foreach(self::$page_plugins as $plugin) {
                    $vars = call_user_func($plugin, $vars);
                }
                echo $this->template_engine->process($template, $vars);
            };
        }
    }
?>
