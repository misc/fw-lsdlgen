<?php
    require_once('vendor/autoload.php');

    use greenscale\server\Config;
%dbuses%
    
    /**
     * generates the actual path to a driver specific SQL script, based on the global conf and the name of the script
     */
    function get_full_sql_path(string $name) : string
    {
        $path_base = sprintf('%s%s%s', __DIR__, DIRECTORY_SEPARATOR, $name);
        $path_common = sprintf('%s.sql', $path_base);
        $path_specific = sprintf('%s.%s.sql', $path_base, Config::get()->db_native->default->driver);
        return (file_exists($path_specific) ? $path_specific : $path_common);
    }

    $tmpconf = tempnam(sys_get_temp_dir(), 'setup');
    $GLOBALS['conf'] = __DIR__.DIRECTORY_SEPARATOR."conf.json";
    echo "[ reading configuration from ".$GLOBALS['conf']." ... ]".PHP_EOL;
    $conf = Config::get();
    if (isset($conf->cli)) {
        $conf->main = $conf->cli;
        file_put_contents($tmpconf, json_encode($conf));
        $GLOBALS['conf'] = $tmpconf;
    }

    $is_silent = false;
    $views_only = false;

    function ask_user ($action, $is_silent) {
        if ($is_silent) {
            return true;
        }
        $answer = readline("Do you really want to commit $action? Type 'yes' if you are sure: ");
        if ($answer === "yes") {
            return true;
        }
        return false;
    }

    if (count($argv) === 1) {
        echo "This script allows you to manage (initialize, fill or reset) database tables.".PHP_EOL.
             "Usage: [script] [OPTIONS]".PHP_EOL.
             "Available Options: force viewsonly clear drop fillbefore create fillafter all".PHP_EOL.
             "Use option 'force' to suppress questions asking for user confirmation.".PHP_EOL.
             "Use option 'viewsonly' to only apply actions clear, drop or create to views.".PHP_EOL.
             "Use option 'all' to execute clear, drop, fillbefore*, create and fillafter* in order.".PHP_EOL.
             "* will not be executed when using option 'viewsonly' additionally".PHP_EOL;
        die();
    }

    try {
%dbinstances%
    }
    catch (Throwable $e) {
        echo "Missing class. Run 'composer dump-autoload' first.".PHP_EOL;
        die();
    }

    for ($i = 1; $i < count($argv); $i++) {
        if ($argv[$i] === "force") {
            $is_silent = true;
        }
        if ($argv[$i] === "viewsonly") {
            $views_only = true;
        }
        if ($argv[$i] === "clear" || $argv[$i] === "all") {
            if (ask_user("clear", $is_silent)) {
                if ($views_only) {
%dbviewclears%
                }
                else {
%dbclears%
%dbviewclears%
                }
            }
        }
        if ($argv[$i] === "drop" || $argv[$i] === "all") {
            if (ask_user("drop", $is_silent)) {
                if ($views_only) {
%dbviewdrops%
                }
                else {
%dbdrops%
%dbviewdrops%
                }
            }
        }
        if ($argv[$i] === "create" || $argv[$i] === "all") {
            if (ask_user("create", $is_silent)) {
                 if ($views_only) {
%dbviewcreates%
                }
                else {
%dbcreates%
%dbviewcreates%
                }
            }
        }
        if ($argv[$i] === "fillbefore" || ($argv[$i] === "all" && !$views_only)) {
            if (ask_user("fillbefore", $is_silent)) {
                $fill = new DatabaseTable(null, "default");
                $before = @file_get_contents(get_full_sql_path('initdb-before'));
                @$fill->query_spread(trim($before));
            }
        }
        if ($argv[$i] === "fillafter" || ($argv[$i] === "all" && !$views_only)) {
            
            if (ask_user("fillafter", $is_silent)) {
                $after = @file_get_contents(get_full_sql_path('initdb-after'));
                @$fill->query_spread(trim($after));
            }
        }
    }
?>
