<?php
    namespace %vendor%\server\%prefix%;

%dbrequires%
    use dibi\dibi;
    use greenscale\server\admin\UserAdminException;
    use greenscale\server\auth\ACLManager;
    use greenscale\server\Config;
%dbuses%
    use greenscale\server\io\Output;
    use greenscale\server\io\OutputConflict;
    use greenscale\server\io\OutputFailedDependency;
    use greenscale\server\io\OutputForbidden;
    use greenscale\server\io\OutputNotImplemented;
    use greenscale\server\io\OutputSuccess;
    use greenscale\server\io\Template;
    use greenscale\server\io\Upload;
    use greenscale\server\mail\MailManager;
    use greenscale\server\router\Route;
    use greenscale\server\service\PostService;

    /**
     * Class %Controller%Controller manages interactions
     * @author          Martin Springwald <springwald@greenscale.de>
     * @license         Greenscale Open Source License
     */
    class %Controller%Controller extends PostService {
        /**
         * Import traits
         */
        use \greenscale\server\database\DatabaseFrame;

%dbvars%

        /**
         * Template Engine
         */
        private $template_engine = null;

        /**
         * Mail Manager
         */
        private $mail_manager = null;

        /**
         * Constructor of class %Controller%Controller initializes Database APIs
         * @param       \Router $router Router
         */
        function __construct ($router) {
            parent::__construct($router, '%prefix%_api', null, true, true, true);
%dbinstances%
            $this->mail_manager = new MailManager();
        }

        /**
         * Translate fields in result row
         * @param       $result
         * @param       $field_list
         */
        private function translateRow (&$row, $field_list) {
            if ($row != null) {
                foreach($field_list as $field) {
                    if (isset($row->{$field})) {
                        $row->{$field} = Template::translate($row->{$field});
                    }
                }
            }
        }

        /**
         * Translate fields in result set
         * @param       $result
         * @param       $field_list
         */
        private function translateResult (&$result, $field_list) {
            if (is_array($result)) {
                foreach($result as $row) {
                    $this->translateRow($row, $field_list);
                }
            }
            else {
                $this->translateRow($result, $field_list);
            }
        }
        
        /**
         * Call action
         * @param       $data_action Action Name
         * @param       $data_payload Input data if available
         * @param       $data_result optional result data
         * @return  {object}
         */
        public function callAction ($data_action, $data_payload, $data_result = null) {
            $result = $this->processMethodRequest((object) array(
                'action' => $data_action,
                'payload' => $data_payload,
                'result' => $data_result
            ));
            if ($result instanceof OutputSuccess) {
                return $result->getPayload();
            }
            if ($result instanceof OutputForbidden) {
                throw new UserAdminException("Forbidden");
            }
            if ($result instanceof OutputNotImplemented) {
                throw new UserAdminException("Not Implemented");
            }
            return $result;
        }

        /**
         * Process method request
         * @param       $data Input data if available
         * @return  \OutputAnswer
         */
        public function processMethodRequest ($data, $plain_result = true) {
            $frame_from = $this->safe_get_frame_from($data);
            $frame_to = $this->safe_get_frame_to($data);
            $user = ACLManager::get_current_user();
            if ($plain_result === false) {
              if (!ACLManager::check($this->get_name(), $data, $user)) {
                return new OutputForbidden($this->allow);
              }
            }
            switch ($data->action) {
                // ACL actions
                case "language": {
                    if (isset($data->payload->lang)) {
                        $_SESSION['lang'] = preg_replace("/[^[:alnum:]]/u", "", $data->payload->lang);
                    }
                    if (!isset($_SESSION['lang'])) {
                        return new OutputSuccess($this->allow, Config::get()->main->language);
                    }
                    else {
                        return new OutputSuccess($this->allow, $_SESSION['lang']);
                    }
                    break;
                }
                case "session": {
                    return new OutputSuccess($this->allow, $user);
                    break;
                }
                case "logout": {
                    $_SESSION['auth'] = false;
                    $_SESSION['user'] = null;
                    return new OutputSuccess($this->allow, true);
                    break;
                }
                case "login": {
                    $result = ACLManager::login((object) $data->payload->model);
                    if ($plain_result === true) {
                        return $result;
                    }
                    else {
                        return $result;
                    }
                    break;
                }
                case "register": {
                    $result = ACLManager::register((object) $data->payload->model);
                    if ($plain_result === true) {
                        return $result->{'user_id'};
                    }
                    else {
                        return new OutputSuccess($this->allow, $result->{'user_id'});
                    }
                    break;
                }
                case "activate": {
                    $result = ACLManager::activate((object) $data->payload->model);
                    if ($plain_result === true) {
                        return $result;
                    }
                    else {
                        return new OutputSuccess($this->allow, $result);
                    }
                    break;
                }
                case "sendmail": {
                    if ($plain_result === true) {
                        $result = $this->mail_manager->sendTemplateMessage(
                            $data->result->subject,
                            $data->result->email,
                            $data->result->template,
                            $data->result
                        );
                        return $result;
                    }
                    else {
                        return new OutputNotImplemented($this->allow);
                    }
                    break;
                }
                // Upload actions
                case "begin_upload": {
                    $result = (object) array(
                        'ticket_id' => Upload::begin_upload(),
                        'size' => Upload::get_max_size()
                    );
                    return new OutputSuccess($this->allow, $result);
                }
                case "continue_upload": {
                    $result = Upload::continue_upload(
                        $data->payload->ticket_id,
                        Upload::decode_slice($data->payload->slice),
                        $data->payload->index
                    );
                    return new OutputSuccess($this->allow, $result);
                }
                case "finish_upload": {
                    $result = Upload::finish_upload(
                        $data->payload->ticket_id
                    );
                    return new OutputSuccess($this->allow, $result);
                }
                // Download actions
                case "download": {
                    $file_name = $data->payload->file_name;
                    $link = "";
                    if (isset($data->payload->link_mode) && ($data->payload->link_mode == "direct")) {
                        $link = Upload::create_perma_link($file_name);
                    }
                    else {
                        $link = Upload::create_random_link($file_name);
                    }
                    $result = (object) array(
                        'link' => str_replace('%server_name%', $_SERVER['SERVER_NAME'], Config::get()->main->base_uri)."dist/".$link,
                        'size' => Upload::get_file_size($file_name),
                        'mime' => $data->payload->file_mime,
                        'name' => $data->payload->file_label
                    );
                    if ($plain_result === true) {
                        return $result;
                    }
                    else {
                        return new OutputSuccess($this->allow, $result);
                    }
                }
                case "download_batch": {
                    $result = null;
                    if (isset($data->payload->batch)) {
                        $result = [];
                        foreach($data->payload->batch as $batch_entry) {
                            $file_name = $batch_entry->file_name;
                            $link = "";
                            $size = 0;
                            if (isset($batch_entry->link_mode) && ($batch_entry->link_mode == "direct")) {
                                try {
                                    $link = Upload::create_perma_link($file_name);
                                }
                                catch (UserAdminException $exception) {
                                    $link = null;
                                }
                                if ($link !== null) {
                                    try {
                                        $size = Upload::get_file_size($file_name);
                                    }
                                    catch (UserAdminException $exception) {
                                        $size = -1;
                                    }
                                }
                            }
                            else {
                                try {
                                    $link = Upload::create_random_link($file_name);
                                }
                                catch (UserAdminException $exception) {
                                    $link = null;
                                }
                                if ($link !== null) {
                                    try {
                                        $size = Upload::get_file_size($file_name);
                                    }
                                    catch (UserAdminException $exception) {
                                        $size = -1;
                                    }
                                }
                            }
                            $result_entry = (object) array(
                                'link' => (!empty($link)) ? str_replace('%server_name%', $_SERVER['SERVER_NAME'], Config::get()->main->base_uri)."dist/".$link : null,
                                'size' => $size,
                                'mime' => $batch_entry->file_mime,
                                'name' => $batch_entry->file_label
                            );
                            array_push($result, $result_entry);
                        }
                    }
                    if ($plain_result === true) {
                        return $result;
                    }
                    else {
                        return new OutputSuccess($this->allow, $result);
                    }
                }
                // General actions
%actions%
                // Default action
                default: {
                    return new OutputNotImplemented($this->allow);
                    break;
                }
            }
        }

        /**
         * Process post request
         * @param       $data Input data if available
         * @return  \OutputAnswer
         */
        public function processPostRequest ($data, $plain_result = false) {
            try {
                return $this->processMethodRequest($data, $plain_result);
            }
            // Exception handling
            catch (\Dibi\DriverException $exception) {
                return new OutputFailedDependency($this->allow, $this->prepareErrorResponse($exception->getMessage()));
            }
            catch (\Dibi\UniqueConstraintViolationException $exception) {
                return new OutputConflict($this->allow);
            }
            catch (UserAdminException $exception) {
                return new OutputFailedDependency($this->allow, $this->prepareErrorResponse($exception->getName()));
            }
        }
    }
?>
