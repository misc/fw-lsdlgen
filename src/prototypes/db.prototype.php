<?php
	namespace %vendor%\server\database;
	
	use greenscale\server\database\DatabaseTable;
	
	/**
	 * Class %Table%Database abstracts interactions with '%table%' table in database
	 * Dibi is used as abstraction layer for database query handling
	 * @author Martin Springwald <springwald@greenscale.de>
	 * @license Greenscale Open Source License
	 */
	class %Table%Database extends DatabaseTable {
		/**
		 * Constructor of class %Table%Database
		 */
		public function __construct () {
			parent::__construct("%table%", "default");
		}
		
%methods%

		/**
		 * Get instance
		 * @return \%Table%Database
		 */
		public static function get_instance() {
			return new %Table%Database();
		}
	}
?>
