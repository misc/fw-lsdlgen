<?php
	namespace %vendor%\server;

	require_once('../vendor/autoload.php');
	$GLOBALS['conf'] = realpath(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."setup".DIRECTORY_SEPARATOR."conf.json");

	use greenscale\server\io\Output;
	use greenscale\server\io\OutputBadRequest;
	use greenscale\server\router\Router;
%uses%

	// Create router instance
	$router = new Router();
	// Install routes
%installs%
	// Set default handler
	$router->set_default_handler(function () {
		Output::output(new OutputBadRequest());
	});
	// Match route
	$router->match();
?>
